================ Security ================================

* If diff, add IP to aws security https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#SecurityGroups:sort=groupId 

================ App ================================

cd /Users/HelenDesigner/Documents/VIDFIRE/

grunt build

cd ~/Documents (where private key is)


change config.js ENDPOINT_URI:'http://www.vidfire.ca:1816'

scp -i "webcore1.pem" -r /Users/HelenDesigner/Documents/VIDFIRE/dist/* bitnami@webcore1.com:/opt/bitnami/apache2/htdocs/vidfire

================ API ================================

nano /Users/HelenDesigner/Documents/VIDFIRE/api/server/config.json | change 0.0.0.0 to www.vidfire.ca

{
  "restApiRoot": "/api",
  "host": "0.0.0.0",
  "domain": "www.vidfire.ca",
  "port": 1816,
  "appPort": "", 
  "url": "http://www.vidfire.ca:1816/"
}
*appPort needed only on localhost

{
  "restApiRoot": "/api",
  "host": "0.0.0.0",
  "domain": "0.0.0.0",
  "port": 1816,
  "appPort": ":7018", 
  "url": "http://0.0.0.0:1816"
}

scp -i "webcore1.pem" -r /Users/HelenDesigner/Documents/VIDFIRE/api/client /Users/HelenDesigner/Documents/VIDFIRE/api/common /Users/HelenDesigner/Documents/VIDFIRE/api/definitions /Users/HelenDesigner/Documents/VIDFIRE/api/server bitnami@webcore1.com:/opt/bitnami/apache2/htdocs/vidfire/api/

================ MongoDB ================================


sudo mongodump --db vidfire --out /Users/HelenDesigner/Documents/VIDFIRE/db_backups/vidfire_mongo_`date +"%m-%d-%y"`

scp -i "webcore1.pem" -r /Users/HelenDesigner/Documents/VIDFIRE/db_backups/vidfire_mongo_`date +"%m-%d-%y"` bitnami@webcore1.com:/opt/bitnami/apache2/htdocs/vidfire/api/db_restore

ssh -i "webcore1.pem" bitnami@webcore1.com 

sudo mongorestore --db vidfire --drop /opt/bitnami/apache2/htdocs/vidfire/api/db_restore/vidfire_mongo_`date +"%m-%d-%y"`/vidfire


================ StrongLoop ================================

cd /opt/bitnami/apache2/htdocs/vidfire/api/

once: slc run
background: nohup slc run 2>&1 &
check if slc is running: ps aux | grep -i slc

check strongloop explorer http://www.vidfire.ca:1816/explorer/

****Config must be 0.0.0.0, otherwise won't work on server.

telnet www.vidfire.ca 1816 

================ Rabbit ================================

telnet www.vidfire.ca 15672

http://www.vidfire.ca:15672

??
sudo rabbitmqctl stop
sudo rabbitmq-server stop

start:
sudo rabbitmq-server start

sudo rabbitmq-server status

background start:
sudo nohup rabbitmq-server start  2>&1 &

ps aux | grep -i rabbitmq-server

if need debug:
tail -f /var/log/rabbitmq/rabbit@ip-172-31-14-102.log

this should coonect
wscat -c ws://localhost:15674/ws -p 13
wscat -c ws://www.vidfire.ca:15674/ws -p 13

works: wscat -c ws://192.168.2.36:15674/ws -p 13

files /etc/rabbitmq

[
  {rabbit, [
    {tcp_listeners, [{"::",       5672}]}
  ]}
].


================ Eureka ================================


Check eureka- 

ps aux | grep -i jar

nohup java -jar eureka-service-0.0.1-SNAPSHOT.jar 2>&1 &

http://www.vidfire.ca:8761/

** built Eureka 
~/tmp/gs-service-registration-and-discovery/complete/eureka-service/
sudo mvn package, picked up in /targets folder

================ Login ================================


Test registration - https://temp-mail.org/en/option/change/


============ Change Instance Size ===============
Instance Settings > Change Instance Size

============ wc-video-worker-S3-Bucket ============

AKIAIWEKAK3TODMILFLQ
zoJDZvl6XEwjK0BTU+ls5LhuW2dFzUmSGKoTfwI6

=====Local 

http://0.0.0.0:7018/#/login

right
http://0.0.0.0:1816/api/vfUsers/confirm?uid=595e360a37fb816893bac3aa&redirect=http%3A%2F%2Fwww.vidfire.ca%2F%23%2Flogin&token=0c526035af2548f4e949c5d74dcfd623ae5b1fe8e62a5475a0c8d916080d3c696a1bb9989c22367c520f227e319f8bdcfe17d35b1255aee3c9afdddeff315ddd

http://0.0.0.0:1816/api/Users/confirm?uid=595e360a37fb816893bac3aa&redirect=http://www.vidfire.ca/#/login

var config = require('../../server/config.json');
var path = require('path');

module.exports = function(User) {

console.log('Exports of User started');

  //send verification email after registration
  User.afterRemote('create', function(context, vfUser, next1) {
    console.log('> User.afterRemote triggered');

    console.log(vfUser);
    //console.log(config,context);

/*
var verifyLink = config.url +
      config.restApiRoot +
      '/vfUsers/confirm' +
      '?uid=' +
      vfUser.id +
      '&redirect=' + 'http://' + config.domain + '/#/login' +

      '&token='

      ;*/

      //console.log(verifyLink);
      //http://0.0.0.0:1816/api/vfUsers/confirm?uid=595a565b80cce61010465f5a&redirect=http%3A%2F%2Fwww.vidfire.ca%2F%23%2Flogin&token=248f76575a3404d9724a4908e788610cf7d8ae3d9dd0775994dfd5fbe60e712174b54aaf0907757cedccb617be91304ec998c5c9b9fecc9d955ded0789aa7851

    var options = {
      type: 'email',
      to: vfUser.email,
      from: 'getvidfire@gmail.com',
      subject: 'Thanks for registering.',
      template: path.resolve(__dirname, '../../server/views/verify.ejs'),
      redirect: 'http://' + config.domain + config.appPort + '/#/login',
      user: vfUser
      //verifyHref: verifyLink
    };

     //console.log(options);

    vfUser.verify(options, function(err, response, next) {


      options.verifyHref = options.verifyHref.replace(config.host, config.domain);
      
      console.log('verifyHref',options.verifyHref);



      if (err) return next(err);

      console.log('> verification email sent:', response);
      context.result = {
        status: 'success',
        title: 'Signed up successfully',
        content: 'Please check your email and click on the verification link before logging in.'
      };

      next1();
    });
  });
}
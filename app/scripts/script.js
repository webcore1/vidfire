var App = angular.module('myApp', []);
App.controller('myCtrl', function($scope) {
    //$scope.dom = '<!DOCTYPE foo SYSTEM "Foo.dtd"><a><b>bbb</b><c/><d><soapenv:Envelope xmlns:soapenv="http://xxx" xmlns:xsd="http://yyy" xmlns:xsi="http://zzz"></soapenv></d><e><![CDATA[ <z></z> ]]></e><f><g></g></f></a>';
    $scope.dom = '<rss version=\'2.0\'><channel><title>RSS Title</title></channel>    </rss>'
})

App.directive('prettyprint', function() {
    return {
        restrict: 'C',
        link: function postLink(scope, element, attrs) {
              element.text(vkbeautify.xml(scope.dom, 4));
        }
    };
});

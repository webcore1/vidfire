'use strict';

/**
 * @ngdoc overview
 * @name uiApp
 * @description
 * # uiApp
 *
 * Main module of the application.
 */
angular
    .module('uiApp', [
        'config',
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngMaterial',
        'lbServices',
        'angular-sortable-view',
        'chart.js',
        'oi.select',
        'vAccordion',
        'smart-table',
        'formly',
        'ngMdIcons',
        'formlyMaterial',
        'mdDataTable',
        "ngSanitize",
        "com.2fdevs.videogular",
        "com.2fdevs.videogular.plugins.controls",
        "com.2fdevs.videogular.plugins.overlayplay",
        "com.2fdevs.videogular.plugins.poster",
        "com.2fdevs.videogular.plugins.buffering",
        "ngStomp"
    ])


.filter('nospace', function() {
        return function(value) {
            return (!value) ? '' : value.replace(/ /g, '');
        };
    })
.factory('Data', function(){

    var service = {
        FirstName: '',
        setFirstName: function(name) {
            // this is the trick to sync the data
            // so no need for a $watch function
            // call this from anywhere when you need to update FirstName
            angular.copy(name, service.FirstName); 
        }
    };
    return service;
})


//starting to build menuLink
.factory('menu', [
        '$location',
        '$rootScope',
        '$http',
        '$window',
        'ENV',
        function($location, $rootScope, $http, $window,ENV) {

            function sortByName(a, b) {
                return a.name < b.name ? -1 : 1;
            }

            var self;

            $rootScope.$on('$locationChangeSuccess', onLocationChange);

            var sections = [];

            sections.push(


                /*{

                name: 'Video',
                type: 'toggle',
                icon: 'fa fa-file-video-o',
                customIcon: false,
                permissionId: '*',

                pages: [ */

                {

                    name: 'Burn',
                    type: 'link',
                    url: '#/burn',
                    icon: 'fa fa-upload',
                    customIcon: false,
                    permissionId: '5911ee17b1f501be81e414c3'


                }

                /*]}*/

                );



                sections.push(


                /*{

                name: 'Share',
                type: 'toggle',
                icon: 'fa fa-share',
                customIcon: false,
                permissionId: '*',

                pages: [ */

                        {

                            name: 'Youtube',
                            type: 'link',
                            url: '#/share/youtube',
                            icon: 'fa fa-youtube',
                            customIcon: false,
                            permissionId: '5911ee17b1f501be81e414c3'


                        }

                        /*

                    ]
                }

                */

                );


/*
            sections.push({

                    name: 'Library',
                    type: 'link',
                    url: '#/library',
                    icon: 'fa fa-film',
                    customIcon: false,
                    permissionId: '5911ee17b1f501be81e414c3'

                });
*/

            sections.push({

                    name: 'My Videos',
                    type: 'link',
                    url: '#/jobs',
                    icon: 'fa fa-film',
                    customIcon: false,
                    permissionId: '5911ee17b1f501be81e414c3'

                });


            sections.push({

                    name: 'Templates',
                    type: 'link',
                    url: '#/templates',
                    icon: 'fa fa-file-video-o',
                    customIcon: false,
                    permissionId: '5911ee17b1f501be81e414c3'

                });


            sections.push({
                name: 'Help',
                type: 'link',
                url: '#/help',
                icon: 'fa fa-life-buoy',
                customIcon: false,
                permissionId: '5911ee17b1f501be81e414c3' //Manage CIE Instances 
            });

            /*
            sections.push({
                name: 'Configuration',
                type: 'link',
                url: '#/configuration',
                icon: 'fa fa-wrench',
                customIcon: false,
                permissionId: '5911ee17b1f501be81e414c3' //Manage CIE Instances 
            });
            */

            sections.push({
                name: 'Security',
                type: 'link',
                url: '#/security',
                icon: 'fa fa-shield',
                customIcon: false,
                permissionId: '5911edefb1f501be81e414c0'
            });

            sections.push({
                name: 'Database',
                type: 'link',
                url: ENV.ENDPOINT_URI+'/explorer',
                icon: 'fa fa-database',
                customIcon: false,
                permissionId: '5911ee04b1f501be81e414c1' 
            });

            sections.push({
                name: 'Files',
                type: 'link',
                url: 'https://console.aws.amazon.com/s3/buckets/wc1-video/?region=us-west-2&tab=overview',
                icon: 'fa fa-files-o',
                customIcon: false,
                permissionId: '5911ee04b1f501be81e414c1' 
            });

            sections.push({
                name: 'RabbitMQ',
                type: 'link',
                url: 'http://'+window.location.hostname+':15672/',
                icon: 'fa fa-hand-peace-o',
                customIcon: false,
                permissionId: '5911ee0fb1f501be81e414c2' 
            });


            return self = {
                sections: sections,

                selectSection: function(section) {
                    self.openedSection = section;
                },

                //when toggle is oepned
                toggleSelectSection: function(section) {
                    //console.log("opened section toggleselectsection");
                    self.openedSection = (self.openedSection === section ? null : section);
                    //console.log(self.openedSection);
                },


                isSectionSelected: function(section) {
                    //console.log("===isSectionselected");
                    //console.log(section);
                    //console.log(self.openedSection);
                    return self.openedSection === section;
                },

                selectPage: function(section, page) {
            page && page.url && $location.path(page.url);
            self.currentSection = section;
            self.currentPage = page;
                },
                isPageSelected: function(page) {
                    return self.currentPage === page;
                }
            };

            function onLocationChange() {
                var path = $location.path();
                //alert("location");
                var introLink = {
                    name: "Introduction",
                    url: "/",
                    type: "link"
                };

                if (path == '/') {
                    self.selectSection(introLink);
                    self.selectPage(introLink, introLink);
                    return;
                }

                var matchPage = function(section, page) {
                    if (path.indexOf(page.url) !== -1) {
                        self.selectSection(section);
                        self.selectPage(section, page);
                    }
                };

                sections.forEach(function(section) {
                    if (section.children) {
                        // matches nested section toggles, such as API or Customization
                        section.children.forEach(function(childSection) {
                            if (childSection.pages) {
                                childSection.pages.forEach(function(page) {
                                    matchPage(childSection, page);
                                });
                            }
                        });
                    } else if (section.pages) {
                        // matches top-level section toggles, such as Demos
                        section.pages.forEach(function(page) {
                            matchPage(section, page);
                        });
                    } else if (section.type === 'link') {
                        // matches top-level links, such as "Getting Started"
                        matchPage(section, section);
                    }
                });
            }
        }
    ])
    // add menu fucntions


.constant('forms', {})
.directive('menuLink', function() {
    return {
        scope: {
            section: '='
        },
        templateUrl: 'views/templates/menu-link.tmpl.html',
        link: function($scope, $element) {
            var controller = $element.parent().controller();
            $scope.isSelected = function() {
                //console.log($scope.section);
                return controller.isOpen($scope.section);
            };

            $scope.focusSection = function() {
                // set flag to be used later when
                // $locationChangeSuccess calls openPage()
                controller.autoFocusContent = true;
            };   

        }
    };
})

.filter('contains', function() {
  return function (array, needle) {
    return array.indexOf(needle) >= 0;
  };
})





 .directive('menuToggle', ['$timeout' , '$rootScope' , function($timeout,$rootScope ) {
      return {
        scope: {
          section: '='
        },
        templateUrl: 'views/templates/menu-toggle-tmpl.html',
        link: function (scope, element) {
          var controller = element.parent().controller();

          scope.isOpen = function () {
            return controller.isOpen(scope.section);
          };
          scope.toggle = function () {
            controller.toggleOpen(scope.section);
          };


        scope.checkPermission2 = function(page) {

            var permission = page.permissionId;
            var permissions = $rootScope.permissions[0];

            if(permissions[permission]===true || permission === "*"){
              return true;
            }

        };        

          
          var parentNode = element[0].parentNode.parentNode.parentNode;
          if (parentNode.classList.contains('parent-list-item')) {
            var heading = parentNode.querySelector('h2');
            element[0].firstChild.setAttribute('aria-describedby', heading.id);
          }
        }
      };
    }])

.config(
    function($mdIconProvider, $$mdSvgRegistry) {
        // Add default icons from angular material
        $mdIconProvider
            .icon('md-close', $$mdSvgRegistry.mdClose)
            .icon('md-menu', $$mdSvgRegistry.mdMenu)
            .icon('md-toggle-arrow', $$mdSvgRegistry.mdToggleArrow);
    })

.config(config)
    .run(run);

config.$inject = ['$routeProvider', '$locationProvider'];

function config($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {/*
            controller: 'MainCtrl',
            templateUrl: 'views/index.html',
            controllerAs: 'vm'
            */

          templateUrl: 'views/create.html',
          controller: 'CreateCtrl',
          controllerAs: 'create'
            
        })
        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'views/login.view.html',
            controllerAs: 'vm'
        })
        .when('/register', {
            controller: 'RegisterController',
            templateUrl: 'views/register.view.html',
            controllerAs: 'vm'
        })

        .when('/register/confirm', {
            controller: 'RegisterController',
            templateUrl: 'views/register.confirm.view.html',
            controllerAs: 'vm'
        })


        .when('/security', {
            templateUrl: 'views/security.html',
            controller: 'SecurityCtrl',
            controllerAs: 'security'
        })

        .when('/configuration', {
            templateUrl: 'views/config.html',
            controller: 'ConfigCtrl',
            controllerAs: 'vm'
        })

        .when('/security', {
            templateUrl: 'views/security.html',
            controller: 'SecurityCtrl',
            controllerAs: 'security'
        })

        .when('/jobs', {
            templateUrl: 'views/jobs.html',
            controller: 'JobsCtrl',
            controllerAs: 'jobs'
        })

        .when('/library', {
            templateUrl: 'views/library.html',
            controller: 'LibraryCtrl',
            controllerAs: 'library'
        })
        
        .when('/burn', {
          templateUrl: 'views/create.html',
          controller: 'CreateCtrl',
          controllerAs: 'create'
        })

        .when('/burn/:vidId', {
          templateUrl: 'views/create.form.html',
          controller: 'CreateCtrl',
          controllerAs: 'create'
        })

        .when('/burn/:vidId/:jobId', {
          templateUrl: 'views/create.form.html',
          controller: 'CreateCtrl',
          controllerAs: 'create'
        })

        .when('/burn/:vidId/:jobId/:sceNum', {
          templateUrl: 'views/create.form.html',
          controller: 'CreateCtrl',
          controllerAs: 'create'
        })


        .when('/status/:jobId', {
          templateUrl: 'views/status.html',
          controller: 'StatusCtrl',
          controllerAs: 'status'
        })



        .when('/help', {
          templateUrl: 'views/help.html',
          controller: 'HelpCtrl',
          controllerAs: 'help'
        })
        .when('/templates', {
          templateUrl: 'views/templates.html',
          controller: 'TemplatesCtrl',
          controllerAs: 'templates'
        })

        .when('/templates/:tId', {
          templateUrl: 'views/templates.html',
          controller: 'TemplatesCtrl',
          controllerAs: 'tmp'
        })


        .when('/share/youtube', {
          templateUrl: 'views/share/youtube.html',
          controller: 'ShareYoutubeCtrl',
          controllerAs: 'vm'
        })

        .when('/share/youtube/:vidId', {
          templateUrl: 'views/share/youtube.html',
          controller: 'ShareYoutubeCtrl',
          controllerAs: 'vm'
        })

        .when('/login/reset', {
          templateUrl: 'views/login/reset.html',
          controller: 'LoginResetCtrl',
          controllerAs: 'login/reset'
        })
        .when('/watch/:id', {
          templateUrl: 'views/watch.html',
          controller: 'WatchCtrl',
          controllerAs: 'watch'
        })
        .otherwise({
            redirectTo: '/login'
        });




}

run.$inject = ['$rootScope', '$location', '$cookieStore', '$http','$mdToast','instancesData','$route'];

function run($rootScope, $location, $cookieStore, $http,$mdToast,instancesData,$route,$routeProvider) {


    $rootScope.permissions = $cookieStore.get('permissions');
    console.log('$rootScope.permissions',$rootScope.permissions);

    $rootScope.Rinstances = $cookieStore.get('instances');
    console.log('$rootScope.instances',$rootScope.Rinstances);

    $rootScope.activeInstance = $cookieStore.get('activeInstance');
    console.log('$rootScope.activeInstance',$rootScope.activeInstance);


    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
   

    }

    $rootScope.$on('$locationChangeStart', function(event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        //            alert("$rootScope.globals.locationChangeStart "+event);

        var restrictedPage = $.inArray($location.path(), ['/login', '/register', '/login/reset', '/register/confirm', '/watch/']) === -1;
        console.log($location.path(),restrictedPage);

        //open up watch links
        var path = $location.path();
        if(path.indexOf('/watch/') === 0){
            restrictedPage = false;
        }
        
        var loggedIn = $rootScope.globals.currentUser;
        if (restrictedPage && !loggedIn) {
            $location.path('/login');
            //                alert('test');
        }
    });

    //alert('test');


}


/*Function to change angular model to dot seperated field names*/
function recursify(input){

        var res = {};
        (function recurse(obj, current) {
          for(var key in obj) {
            var value = obj[key];
            var newKey = (current ? current + "." + key : key);  // joined key with dot
            if(value && typeof value === "object") {
              recurse(value, newKey);  // it's a nested object, so do it again
            } else {
              res[newKey] = value;  // it's not an object, so set the property
            }
          }
        })(input);

        return res;

}

function replacer(key, value) {
  if (typeof value === "number") { return String(value); }
  else { return value; }
}

function uncapitalizeFirstLetter(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:SecurityclassCtrl
 * @description
 * # SecurityclassCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')

.config(['$mdIconProvider', function($mdIconProvider) {
    $mdIconProvider.icon('md-close', 'img/icons/ic_close_24px.svg', 24);
}])

.controller('SecurityclassCtrl', DemoCtrl);

function DemoCtrl($timeout, $q) {
    var self = this;

    self.readonly = false;

    // Lists of fruit names and Vegetable objects
    self.fruitNames = ['Secret', 'Confidential', 'Internal'];
    self.roFruitNames = angular.copy(self.fruitNames);
    self.editableFruitNames = angular.copy(self.fruitNames);

    self.tags = [];
    self.vegObjs = [{
        'name': 'Secret',
        'type': 'Brassica'
    }, {
        'name': 'Confidential',
        'type': 'Brassica'
    }, {
        'name': 'Internal',
        'type': 'Umbelliferous'
    }];

    self.newVeg = function(chip) {
        return {
            name: chip,
            type: 'unknown'
        };
    };
}
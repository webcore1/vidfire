'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:RegisterControllerCtrl
 * @description
 * # RegisterControllerCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
    .controller('RegisterController', RegisterController);

RegisterController.$inject = ['UserService', '$location', '$rootScope', 'FlashService', 'AuthenticationService'];

function RegisterController(UserService, $location, $rootScope, FlashService, AuthenticationService) {
    
    var vm = this;
    vm.register = register;
    vm.errors = [];
    vm.model = {};
    vm.options = {};
    vm.fields = [


        {
            className: '',
            //hideExpression: "model.sourceType != 2",
            fieldGroup: [

                {
                    className: 'md-block',
                    key: "name",
                    type: 'input',
                    templateOptions: {
                        type: 'text',
                        label: 'Name',
                        placeholder: '',
                        required: true
                    }

                }

            ]
        },

        {
            className: '',
            //hideExpression: "model.sourceType != 2",
            fieldGroup: [

                {
                    className: 'md-block',
                    key: "email",
                    type: 'input',
                    templateOptions: {
                        type: 'email',
                        label: 'Email',
                        placeholder: '',
                        required: true
                    }

                }

            ]
        },

        {
            className: '',
            //hideExpression: "model.sourceType != 2",
            fieldGroup: [

                {
                    className: 'md-block',
                    key: "password",
                    type: 'input',
                    templateOptions: {
                        type: 'password',
                        label: 'Password',
                        placeholder: '',
                        required: true,
                        minlength: 5,
                        maxlength: 10
                    }

                }

            ]
        }

    ]


    function register() {

        vm.options.updateInitialValue();

        vm.dataLoading = true;

        // Poor security fix asap.
        vm.model.group = "591c8896fe6ef30482293ef2";

        UserService.Create(vm.model)
            //AuthService.register($scope.user.email, $scope.user.password)            
            .then(function(response) {

                console.log(response);

                if (response.status === "success") {
                    //console.log('registered', response);
                    //FlashService.Success('Registration successful', true);
                    $location.path('/register/confirm');
                    console.log(response);
                    //login();
                } else {

                    //alert('error');
                    FlashService.Error(response.message);
                    //vm.dataLoading = false;
                }
            }).catch(function(error) {


                //console.log(error);

                angular.forEach(error.data.error.details.messages, function(val, key) {

                    console.log(val, key);

                    angular.forEach(val, function(item) {

                        vm.errors.push(item);


                    })



                })

                //console.log(error.data.error.details.messages);



            });
    }

    function login() {

        AuthenticationService.Login(vm.model.email, vm.model.password, function(response) {

            console.log(response);
            if (response.id) {
                AuthenticationService.SetCredentials(vm.model.email, response.id);
                $location.path('/burn');
            } else {

                FlashService.Error(response.message);
                //alert(response.message);
                alert('Login Failed!');
                //vm.dataLoading = false;
            }

        });
    };


}
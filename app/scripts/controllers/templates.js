'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:TemplatesCtrl
 * @description
 * # TemplatesCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('TemplatesCtrl', function ($routeParams,$route,$scope,api,FlashService,$rootScope,$location) {

  	$scope.pagetitle = 'Templates';
  	$scope.icon = 'file-video-o';

  	console.log('$routeParams',$route);

  	$scope.currentUser = $rootScope.globals.currentUser.username;

	var tmp = this;
	tmp.errors = [];
	tmp.model = {};
	tmp.options = {};
	tmp.save = save;
	tmp.fields = [

	        {
	            className: '',
	            fieldGroup: [

	                {
	                    className: 'md-block',
	                    key: "name",
	                    type: 'input',
	                    templateOptions: {
	                        type: 'text',
	                        label: 'Name',
	                        placeholder: '',
	                        required: true
	                    }

	                }

	            ]
	        },

	        {
	            className: '',
	            fieldGroup: [

	                {
	                    className: 'md-block',
	                    key: "thumb",
	                    type: 'input',
	                    templateOptions: {
	                        type: 'text',
	                        label: 'Thumbnail',
	                        placeholder: '',
	                        required: true
	                    }

	                }

	            ]
	        },

	        {
	            className: '',
	            fieldGroup: [

	                {
	                    className: 'md-block',
	                    key: "video",
	                    type: 'input',
	                    templateOptions: {
	                        type: 'text',
	                        label: 'Video',
	                        placeholder: '',
	                        required: true
	                    }

	                }

	            ]
	        },

	        {
	            className: '',
	            fieldGroup: [

	                {
	                    className: 'md-block',
	                    key: "videoType",
	                    type: 'input',
	                    templateOptions: {
	                        type: 'text',
	                        label: 'Video Type',
	                        placeholder: '',
	                        required: true
	                    }

	                }

	            ]
	        },


	        {
	            className: '',
	            fieldGroup: [

	                {
	                    className: 'md-block',
	                    key: "template",
	                    type: 'textarea',
	                    templateOptions: {
	                        type: 'text',
	                        label: 'Template JSON',
	                        placeholder: '',
	                        required: true
	                    }

	                }

	            ]
	        }
	    ]


   $scope.deleteTemplate = function(){

    	api.delete(templateID).
			then(function(res) {
				console.log('tempate',res);
				if(res.data.count > 0){

					FlashService.Success('Deleted', true);
					$location.path('/templates/');

				}else{
					FlashService.Success('Nothing to delete!', true);
				}
		}).catch();

   }


    var templateID = $routeParams.tId;

    if (!templateID) {

    	$scope.list = true;

		api.all().
			then(function(res) {
				console.log(res);
		    $scope.jobsList = res.data;
		}).catch();
    
    }


    if (templateID && templateID !== "add") {

    	$scope.edit = true;

    	api.one(templateID).
			then(function(res) {
				console.log('tempate',res);
				$scope.modified_by = res.data.modified_by;
				$scope.date_modified = res.data.date_modified;
				tmp.model.name = res.data.name;
				tmp.model.thumb = res.data.thumb;
				tmp.model.video = res.data.video[0].src;
				tmp.model.videoType = res.data.video[0].type;
				tmp.model.template = angular.toJson(res.data.template,2);
		    	$scope.job = res.data;
		}).catch();

    }

    if (templateID && templateID === "add") {

    	$scope.edit = true;
    	templateID = "";
    	//alert('add here');
    }	



function save() {

            tmp.options.updateInitialValue();

            //var cJson  = tmp.model.template;

           	//cJson = cJson.trim();

           	//cJson = cJson.replace(/\r?\n|\r/g, " ");


            var pay = angular.copy(tmp.model);

            pay.date_modified = Date();
            pay.modified_by = $scope.currentUser;
			$scope.modified_by = pay.modified_by;
			$scope.date_modified = pay.date_modified;

			pay.thumb = tmp.model.thumb;

			pay.video = [{
				
				src : tmp.model.video,
				type: tmp.model.videoType

			}]

            pay.template = angular.fromJson(pay.template);

            api.putTemplate(templateID,pay)

                .then(function (res) {

                    console.log(res);

                    if (res.status === 200) {

                        FlashService.Success('Saved', true);

                        if(templateID===""){

                        	$location.path('/templates/'+res.data.id);

                        }

                    } else {
                        FlashService.Error(res.message);
                    }

                }).catch(function(error){

                        console.log(error);

                        tmp.errors.push(error.data.error.message);

                        angular.forEach(error.data.error.details.messages,function(val,key){

                            console.log(val,key);

                            angular.forEach(val,function(item){

                                tmp.errors.push(item);

                            })

                        })

                });
        }

});

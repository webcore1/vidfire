'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:LoginControllerCtrl
 * @description
 * # LoginControllerCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
    .controller('LoginController', LoginController);

LoginController.$inject = ['$rootScope', '$location', 'AuthenticationService', 'FlashService','instancesData'];

function LoginController($rootScope, $location, AuthenticationService, FlashService,instancesData) {
    var vm = this;

    vm.login = login;

    (function initController() {
        // reset login status
        AuthenticationService.ClearCredentials();
    })();

    function login() {
        vm.dataLoading = true;
        AuthenticationService.Login(vm.email, vm.password, function(response) {

            console.log(response);
            if (response.id) {
                AuthenticationService.SetCredentials(vm.email, response.id);
                $location.path('/burn');
            } else {

                FlashService.Error(response.message);
                //alert(response.message);
                //alert('Login Failed!');
                vm.dataLoading = false;
            }

        });
    };

}
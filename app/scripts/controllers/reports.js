'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:ReportsCtrl
 * @description
 * # ReportsCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('ReportsCtrl', function ($scope) {

  	$scope.pagetitle = 'Reports';
  	$scope.icon = 'bar-chart';

  });

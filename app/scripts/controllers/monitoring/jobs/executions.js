'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:MonitoringJobsExecutionsCtrl
 * @description
 * # MonitoringJobsExecutionsCtrl
 * Controller of the uiApp
 * This contoller maps jobs table
 */
angular.module('uiApp')
    .controller('MonitoringJobsExecutionsCtrl', function($rootScope, $scope, $sce, $route, $http, SCDF, $mdToast, $routeParams, toast,instancesData) {

        $scope.pagetitle = 'Monitoring';
        $scope.icon = 'eye';
        //$scope.error = 'Spring Server Dashboard is not available, please contact the admin.';
        $scope.jobs;
        var type = $routeParams.type;

        if(!type){
            type = "ingest";
        }

        $scope.type = type;

        function catchError (reply) { 

        var message = "";

        if (reply.status === -1) {

            message = "No Internet Connection!";

        }else if(reply.status === 404){

            message ="Looks like the back-end is down. Contact your Administrator!";


        }else{

            //message = reply.data.meta.errorMessage;
            message = 'Failed to load!';


        }  

        console.log('failed-catch',reply);

        toast.fire(message,'error');

        };



                //get current instance
        //instancesData.all().
        //then(function(response) {
            //alert('4');
            //angular.forEach(response.data, function(val, prop) {
                //if (val.selected === true) {


                            //$rootScope.activeInstance = { name: val.name, uri: val.ip };



                            SCDF.allJobs(type)

                            .success(function(response) {

                                $scope.jobs = response['_embedded']['jobExecutionResourceList'];
                                console.log('jobs:', $scope.jobs);
                                //$scope.error = "";

                            })

                            .catch(catchError);






                    //$rootScope.activeInstance = { name: val.name, uri: val.ip };
                //}
            //});
        //});








        $scope.selectedRowCallback = function(rows) {
            $mdToast.show(
                $mdToast.simple()
                .content('Selected row id(s): ' + rows)
                .hideDelay(3000)
            );
        };

    });
'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:MonitoringJobsJobCtrl
 * @description
 * # MonitoringJobsJobCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('MonitoringJobsStatusCtrl', function($scope, $rootScope, $sce, $route, $routeParams, $http, SCDF, $mdToast, toast, instancesData) {

        var id = $routeParams.id;
        var type = $routeParams.type;
        
        $scope.jobType = type.capitalize();
        $scope.jobSubId = id;
        $scope.pagetitle = 'Monitoring';
        $scope.icon = 'eye';
        $scope.job;



        function catchError (reply) { 

        var message = "";

        if (reply.status === -1) {

            message = "No Internet Connection!";

        }else if(reply.status === 404){

            message ="Looks like the back-end is down. Contact your Administrator!";


        }else{

            //message = reply.data.meta.errorMessage;
            message = 'Failed to load!';


        }  

        console.log('failed-catch',reply);

        toast.fire(message,'error');

        };


        SCDF.fetchJobStatus(id,type)

        .success(function(response) {

            $scope.job = response;
            console.log('job:', $scope.job);

        }).catch(catchError);



        $scope.selectedRowCallback = function(rows) {
            $mdToast.show(
                $mdToast.simple()
                .content('Selected row id(s): ' + rows)
                .hideDelay(3000)
            );
        };

    });
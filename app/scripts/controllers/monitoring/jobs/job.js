'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:MonitoringJobsJobCtrl
 * @description
 * # MonitoringJobsJobCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('MonitoringJobsJobCtrl', function($scope, $rootScope, $sce, $route, $routeParams, $http, SCDF, $mdToast,toast,instancesData) {

        var jobId = $routeParams.jobId;
        var type = $routeParams.type;

        $scope.pagetitle = 'Monitoring';
        $scope.icon = 'eye';
        $scope.error = 'Spring Server Dashboard is not available, please contact the admin.';
        $scope.job;

  function catchError (reply) { 

        var message = "";

        if (reply.status === -1) {

            message = "No Internet Connection!";

        }else if(reply.status === 404){

            message ="Looks like the back-end is down. Contact your Administrator!";

        }else{

            //message = reply.data.meta.errorMessage;
            message = 'Failed to load!';


        }  

        console.log('failed-catch',reply);

        toast.fire(message,'error');

        };


        SCDF.fetchJob(jobId,type)

        .success(function(response) {

            $scope.job = response;
            console.log('job:', $scope.job);
            $scope.error = "";

        })

         .catch(catchError);


        $scope.selectedRowCallback = function(rows) {
            $mdToast.show(
                $mdToast.simple()
                .content('Selected row id(s): ' + rows)
                .hideDelay(3000)
            );
        };

    });
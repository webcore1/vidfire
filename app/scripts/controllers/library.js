'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:LibraryCtrl
 * @description
 * # LibraryCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('LibraryCtrl', function ($scope,api) {

  	$scope.pagetitle = 'Library';
  	$scope.icon = 'film';
  	$scope.videosList = null;
  	api.getAllvideos().
		then(function(response) {
			
			console.log(response);

	    $scope.videosList = response.data;

	}).catch();

  });

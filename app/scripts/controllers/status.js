'use strict';
/**
 * @ngdoc function
 * @name uiApp.controller:DataflowCtrl
 * @description
 * # DataflowCtrl
 * Controller of the uiApp
 */

angular.module('uiApp')
    .controller('StatusCtrl', function(

        $log, $stomp, $window, $scope, $rootScope, $sce, $route, $routeParams, $http, $location, api, ENV

        ) {


        $stomp.setDebug(function(args) {
            console.log(args);
            $log.debug(args)
        })

        var status = this;
        var user = $rootScope.globals.currentUser.username;
        var jobId = $routeParams.jobId;

        status.model = {};

        $scope.pagetitle = 'Job Status';
        $scope.icon = 'font-awesome';
        $scope.thisJob;
        $scope.jStat;
        $scope.messages = [];
        $scope.jobId = jobId;


        if (jobId) {
            api.getJob(jobId).
            then(function(response) {

                $scope.thisJob = response.data;
                $scope.thisJob.template.payload.jobID = jobId;
                console.log('$scope.thisJob', $scope.thisJob);
                $scope.jStat = $scope.thisJob.status;


                //check if new and submit job
                check_submit_new();

                //if youtube sharing exists
                if($scope.thisJob.share) if($scope.thisJob.share.youtube) check_youtube_token();


            }).catch();
        }else{

            $location.path('/404');

        }


        //connect to web socket

            var ws = new WebSocket('ws://' + ENV.HOST + ':15674/ws');
            var client = Stomp.over(ws);

            var connectHeaders = {
                login: 'vf_admin',
                passcode: 'F#F#@OIFN#oi23foi23'
            };


            function coming() {
                alert("Feature coming soon!");
            }



            var on_connect = function() {

                console.log('on_connect');

                var subscription = client.subscribe('/topic/job.' + jobId, function(payload) {


                    console.log('subscription made');

                        //incoming messages post to view
                        if (payload.command === "MESSAGE" && payload.body.length > 0) {

                            var message = payload.body;

                            //check_youtube_token();

                            //var restrictedPage = $.inArray($location.path(), ['/login', '/register', '/login/reset', '/register/confirm', '/watch/']) === -1;


                            if (payload.headers.type === "job_payload" ){

                                //alert('1');

                                console.log("job_payload", message);

                                message = [];
                                message.status = "queued";
                                message.jobName = $scope.thisJob.title;

                                //message = "Your job " + $scope.thisJob.title + " is now in the que, and will be ready soon!";
                                console.log('mark jStat submmited');


                            }else if(payload.headers.type === "youtube"){

                                //alert('2');

                                ////////


                                console.log("youtube", message);

                                message = [];
                                message.status = "youtube-queued";
                                message.jobName = $scope.thisJob.title;


                            }else{

                                //alert('3');


                                message = JSON.parse(message);

                                console.log("job_payload_else", message);

                                //var humanMessage = msg_extractor(message);

                                //console.log("humanMessage", humanMessage);

                                //if(humanMessage != undefined){
                                  //  message = humanMessage;
                                //}

                            }
                    

                            $scope.messages.push(message);
                            $scope.$apply();
                            console.log($scope.messages);
                        }

                        $scope.payload = payload;
                        //alert('123');

                        console.log("payload", payload);

                    }, {
                        'auto-delete': false,
                        'durable': true,
                        //'exclusive': false,
                        'persistent': true,
                        'id': jobId,
                        //'ack': 'client',
                        //'selector': "type = 'status'"
                    }

                )



            };//end on_connect

            var on_error = function() {
                console.log('error');
            };

            client.connect('vf_admin', 'F#F#@OIFN#oi23foi23', on_connect, on_error, '/');


        //}



        // YOUTUBE TOKEN


        function check_submit_new(){

                //submit new stuff here.
                //submit the job if it's new
                console.log('pre-new');
                if ($scope.jStat === "new") {

                    console.log('$scope.jStat === "new"');

                    //submit to master jobs que for processing
                    submit_to_que( $scope.thisJob ,'vidfire.jobs.master.routing' , jobId , 'job_payload');
                
                    //submit to unique job que for records etc
                    client.send('/topic/job.' + jobId, {
                            priority: 1,
                            type: 'job_payload'
                        },
                        JSON.stringify($scope.thisJob));
                

                    $scope.jStat === "queued";

                }

        }


        function submit_to_que(pl,route,jobId,type){

            //var topic = '/topic/job.'+type+'.'+jobId;

            var topic = '/topic/' + route;

            client.send(topic, {
                    priority: 1,
                    type: type
                },
                JSON.stringify(pl));

                console.log('Submit_to_que: ' , topic);

            //$scope.jStat === type;

        }

        function check_youtube_token(){

                if($scope.thisJob.share.youtube.status == "submitted" && $window.localStorage['youtube_token']){

                    var message = [];

                    //alert('youtube_token');

                    console.log('youtube_token',$window.localStorage['youtube_token']);

                    var youtube_token = JSON.parse($window.localStorage['youtube_token']);

                    var time_now = Math.floor(Date.now() / 1000);

                    //alert(time_now);

                    if(time_now > youtube_token.time_received+3600){
                        //alert('expired');
                       message.youtubeToken = 'expired';
                    }else{
                        //alert('good');
                        message.youtubeToken = 'good';

                        var pl = {

                            id: jobId,
                            uri: $scope.thisJob.video.phone,
                            token: youtube_token.oauth.access_token,
                            title: $scope.thisJob.share.youtube.title,
                            description: $scope.thisJob.share.youtube.description,
                            tags: $scope.thisJob.share.youtube.tags
                        }

                        console.log(pl);

                        submit_to_que( pl ,'vidfire.youtube.master.routing' , jobId , 'youtube');
                    }

                    $scope.messages.push(message);

                }   

        }


    });
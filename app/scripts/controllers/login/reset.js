'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:LoginResetCtrl
 * @description
 * # LoginResetCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('LoginResetCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:HelpCtrl
 * @description
 * # HelpCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('HelpCtrl', function ($scope) {

        $scope.pagetitle = 'Help';
        $scope.icon = 'life-buoy';


  });

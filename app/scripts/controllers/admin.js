'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('AdminCtrl', function ($scope) {
  	
  	$scope.pagetitle = 'Admin';
  	$scope.icon = 'university';
    
  });

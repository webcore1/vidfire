'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the uiApp
 */
angular.module('uiApp').directive('myDirective', ['$window', '$timeout', function($window, $timeout) {
        return {
            link: link,
            restrict: 'E'
                //template: '<div>window size: {{width}}px</div>'
        };

        function maxW(scope, element) {
            scope.widgetPxWidth = element[0].offsetWidth;
            //console.log(scope.widgetPxWidth);
            element.css({
                'max-width': scope.widgetPxWidth + 'px'
            });
        };

        function link(scope, element, attrs) {
            $timeout(function() {
                maxW(scope, element);
            }, 2000);

            angular.element($window).bind('resize', function() {
                maxW(scope, element);
                // manuall $digest required as resize event is outside of angular
                scope.$digest();
            });
        }

    }])
    .controller('MainCtrl', function($scope, $timeout) {

        var vm = this;

        $scope.pagetitle = 'Dashboard';
        $scope.icon = 'tachometer';


        $scope.addNewWidget = function(id) {
            console.log('last');
            console.log(id);
            var newWidget = $scope.widget;
            var lenght = $scope.widgets.length;
            newWidget.id = length;
            console.log(newWidget);
            $scope.widgets.push(newWidget);
        };


        $scope.deleteWidget = function(item, index) {
            console.log('widget');
            console.log(item.id);
            $scope.widgets[item.id].status = false;
            //var object_by_id = $filter('filter')(foo.results, {id: item.id })[0];
        };

        $scope.checkStatus = function(status) {
            if (status === false)
                return "hiddenWidget";
        }

        $scope.delete = function(idx) {

            var delPerson = $scope.persons[idx];

            API.DeletePerson({
                id: delPerson.id
            }, function(success) {

                $scope.persons.splice(idx, 1);
            });
        };

        $scope.widgetTypes = ['monitoring', 'ingestion', 'metadata', 'onboard', 'ingest'];

        $scope.selectedNewWidget = "";

        $scope.widget = {
            name: 'New Widget',
            width: 31,
            height: 400,
            active: true,
            category: '',
            payload: '',
            status: true,
            template: 'views/widgets/new.html'
        };

        $scope.widgets = [{
                id: 0,
                name: 'Ingestion #1 Today',
                width: 31,
                height: 400,
                active: true,
                category: '',
                payload: {
                    "label": ["XML", "JSON", "COBOL"],
                    "data": [8005, 10034, 5030]
                },
                status: true,
                template: 'views/widgets/pie.html'
            }, {
                id: 1,
                name: 'Ingestion #1 Weekly Stats',
                width: 62,
                height: 400,
                active: true,
                category: '',
                payload: {
                    'labels': ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],

                    'data': [
                        [34411, 36232, 35194, 37234, 35499, 39331, 37436],
                        [34389, 36209, 35145, 37156, 35423, 39309, 37401]
                    ],

                    'override': [{
                        label: "Files Ingestion Requests",
                        borderWidth: 1,
                        type: 'bar'
                    }, {
                        label: "Files Ingested Sucessfully",
                        borderWidth: 3,
                        hoverBackgroundColor: "rgba(255,99,132,0.4)",
                        hoverBorderColor: "rgba(255,99,132,1)",
                        type: 'line'
                    }]
                },
                status: true,
                template: 'views/widgets/ingestion.html'
            }, {
                id: 2,
                name: 'Ingestion Engine Instances',
                width: 31,
                height: 400,
                active: true,
                category: '',
                payload: '',
                status: true,
                template: 'views/widgets/instances.html'
            }, {
                id: 3,
                name: 'Ingestion #1 Yesterday',
                width: 31,
                height: 400,
                active: true,
                category: '',
                payload: {
                    "label": ["XML", "JSON", "COBOL", "CSV"],
                    "data": [3005, 9034, 3230, 2343]
                },
                status: true,
                template: 'views/widgets/pie.html'
            }, {
                id: 4,
                name: 'Real-Time Sources',
                width: 31,
                height: 400,
                active: true,
                category: '',
                payload: {
                    "label": ["XML", "JSON", "COBOL", "CSV"],
                    "data": [3005, 9034, 3230, 2343]
                },
                status: true,
                template: 'views/widgets/live.html'
            }

            , {
                id: 54,
                name: 'Security',
                width: 31,
                height: 400,
                active: true,
                category: '',
                payload: '',
                status: true,
                template: 'views/widgets/security.html'
            }

        ];


        $scope.colors = ['#76BFF3', '#BFE3E9', '#ffa441', '#3e9fe4', '#f7b66f'];



    });
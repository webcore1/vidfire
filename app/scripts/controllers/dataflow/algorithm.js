'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:DataflowAlgorithmCtrl
 * @description
 * # DataflowAlgorithmCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
    .controller('DataflowAlgorithmCtrl', DataflowAlgorithmCtrl);

DataflowAlgorithmCtrl.$inject = ['$scope', 'algoServ', '$mdToast'];

function DataflowAlgorithmCtrl($scope, algoServ, $mdToast) {

    $scope.pagetitle = 'Onboard Algorithms';
    $scope.icon = '';

     $scope.altImg = '<img src="images/icons/algo_onboard_icon_white.png">';



    $scope.deleteRowCallback = function(rows) {
        $mdToast.show(
            $mdToast.simple()
            .content('Deleted row id(s): ' + rows)
            .hideDelay(3000)
        );
    };

    $scope.test123;


    algoServ.resources().then(function(result) {
        $scope.resourcesList = result.data;
    });


    console.log($scope.resourcesList);

    var algo = this;

    algo.languagesList = ['test', 'test123', 'test1234'];

    algoServ.languages().then(function(result) {
        //algo.languagesList = result.data;
        console.log(algo.languagesList);
    });

    algo.feedUriTypes = [{
        id: 1,
        name: 'hdfs'
    }, {
        id: 2,
        name: 'sftp'
    }, {
        id: 3,
        name: 'ftp'
    }, {
        id: 4,
        name: 'folder'
    }];

    algo.sourceTypes = [{
        id: 1,
        name: 'Code'
    }, {
        id: 2,
        name: 'Service'
    }];

    algo.serviceTypes = [{
        id: 1,
        name: 'soap'
    }, {
        id: 2,
        name: 'rest'
    }];

    algo.fields = [

        {
            className: 'display-flex',
            fieldGroup: [

                {
                    className: 'flex-1',
                    key: "sourceType",
                    type: 'select',


                    templateOptions: {
                        label: 'Select source type',
                        theme: "custom",
                        //inputContainer: "md-block",
                        multiple: false,
                        labelProp: "name",
                        valueProp: "id",
                        required: true,
                        //className: "md-block",
                        "options": algo.sourceTypes
                    }


                },

                {
                    className: 'flex-1',
                    key: "serviceType",
                    type: 'select',


                    templateOptions: {
                        label: 'Select service type',
                        theme: "custom",
                        //inputContainer: "md-block",
                        multiple: false,
                        labelProp: "name",
                        valueProp: "id",
                        required: true,
                        //className: "md-block",
                        "options": algo.serviceTypes
                    },

                    hideExpression: "model.sourceType != 2"

                },

            ]
        },


        {

            type: "fieldGroupTitle",
            templateOptions: {
                label: 'Source:'
            },

            hideExpression: "model.sourceType != 1"

        },



        {
            className: 'display-flex',
            //hideExpression: "model.sourceType != 2",
            fieldGroup: [

                {
                    className: 'flex-1 md-block',
                    key: "mdUriTypeSelected",
                    type: 'select',


                    templateOptions: {
                        label: 'Location Type',
                        theme: "custom",
                        //inputContainer: "md-block",
                        multiple: false,
                        labelProp: "name",
                        valueProp: "id",
                        required: true,
                        //className: "md-block",
                        "options": algo.feedUriTypes
                    },

                    hideExpression: "model.sourceType != 1"


                }, {
                    className: 'flex-4',
                    key: "metadataUri",
                    type: 'input',
                    templateOptions: {
                        type: 'text',
                        label: 'Enter a direct path to your folder with your source files',
                        placeholder: 'PATH TO FILES',
                        required: true
                    },

                    hideExpression: "model.sourceType != 1"

                },



            ]
        },

    ];

    algo.originalFields = angular.copy(algo.fields);

    algo.onSubmit = onSubmit;
    algo.model = {};
    algo.options = {};



    // function definition
    function onSubmit() {

        $scope.showResourcesTable = true;

        //loading graphic
        //algo.loading = true; 

        algo.options.updateInitialValue();
        //alert('added');
        apiMessage('Source Onboarding started', 'reg');
        //onboardservice.saveOnboardSource(algo.model);
        algo.saveOnboardSource();
        //algo.model = {};
        //algo.options.resetModel();
    }

};
'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:ShareYoutubeCtrl
 * @description
 * # ShareYoutubeCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('ShareYoutubeCtrl', function ($scope,$window,$routeParams,api) {
 
	$scope.pagetitle = 'Share on Youtube';
	$scope.icon = 'youtube';
  var vidId = $routeParams.vidId;

  $scope.thisJob = [];

  console.log($routeParams);


    api.getJob(vidId).
    then(function(response) {

        $scope.thisJob = response.data;

        console.log('$scope.thisJob',$scope.thisJob);

        vm.model.title = $scope.thisJob.title;
        


      for (var key in $scope.thisJob.video) {
        if ($scope.thisJob.video.hasOwnProperty(key)) {
          //alert(key); // 'a'
          $scope.thisJob.video = $scope.thisJob.video[key]; // 'hello'
        }
      }  

      vm.model.uri = $scope.thisJob.video;


    }).catch();




    var vm = this;
    vm.errors = [];
    vm.model = {};
    vm.options = {};
    vm.fields = [


        {
            className: '',
            //hideExpression: "model.sourceType != 2",
            fieldGroup: [

                {
                    className: 'md-block',
                    key: "uri",
                    type: 'input',
                    templateOptions: {
                        type: 'text',
                        label: 'Video File',
                        placeholder: '',
                        required: true
                    }

                }

            ]
        },{
            className: '',
            //hideExpression: "model.sourceType != 2",
            fieldGroup: [

                {
                    className: 'md-block',
                    key: "title",
                    type: 'input',
                    templateOptions: {
                        type: 'text',
                        label: 'Title',
                        placeholder: '',
                        required: true
                    }

                }

            ]
        },

        {
            className: '',
            //hideExpression: "model.sourceType != 2",
            fieldGroup: [

                {
                    className: 'md-block',
                    key: "desc",
                    type: 'textarea',
                    templateOptions: {
                        label: 'Description',
                        placeholder: '',
                        rows: 1,
                        required: true
                    }

                }

            ]
        },

        {
            className: '',
            //hideExpression: "model.sourceType != 2",
            fieldGroup: [

                {
                    className: 'md-block',
                    key: "tags",
                    type: 'textarea',
                    templateOptions: {
                        label: 'Tags (e.g., albert einstein, space monkey, macintosh)',
                        placeholder: '',
                        rows: 1,
                        required: false
                    }

                }

            ]
        }

    ];



vm.onSubmit = onSubmit;



function onSubmit() {

  var tags_obj = (vm.model.tags) ? vm.model.tags.split(',') : undefined;

  var pl = { 
    "id": vidId,
    "share": {
    "youtube": {
      "uri": vm.model.uri,
      "date_share_request": Date(),
      "status": "submitted",
      "title": vm.model.title,
      "description": vm.model.desc,
      "tags": tags_obj
      }
    }
  }

  console.log(pl);

  api.putJob(pl).
    then(function(res) {

        if(res.status === 200){

          //console.log('123');

            //$location.path('/burn/'+vidId+'/'+jobId+'/'+$scope.nextSce);

             $scope.oauthSignIn(vidId);

        }

    });
 
}

 $scope.oauthSignIn = function(vidId) {

  // Google's OAuth 2.0 endpoint for requesting an access token
  var oauth2Endpoint = 'https://accounts.google.com/o/oauth2/v2/auth';

  // Create <form> element to submit parameters to OAuth 2.0 endpoint.
  var form = document.createElement('form');
  form.setAttribute('method', 'GET'); // Send as a GET request.
  form.setAttribute('action', oauth2Endpoint);

  // Parameters to pass to OAuth 2.0 endpoint.
  var params = {'client_id': '1018949862977-qj3se2jnfubm1j158577fnfrn94ec8e4.apps.googleusercontent.com',
                'redirect_uri': 'http://'+window.location.host+'/auth.html',
                'response_type': 'token',
                'scope': 'https://www.googleapis.com/auth/youtube.upload',
                'include_granted_scopes': 'true',
                'state': 'http://'+window.location.host+'/#/status/'+vidId
            }

            console.log(params);

  // Add form parameters as hidden input values.
  for (var p in params) {
    var input = document.createElement('input');
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', p);
    input.setAttribute('value', params[p]);
    form.appendChild(input);
  }

  // Add form to page and submit it to open the OAuth 2.0 endpoint.
  document.body.appendChild(form);
  form.submit();
}

//var 3rd_party_token = window.localStorage;

console.log('youtube_token',$window.localStorage['youtube_token']);

/*

https://accounts.google.com/o/oauth2/v2/auth?client_id=1018949862977-qj3se2jnfubm1j158577fnfrn94ec8e4.apps.googleusercontent.com%09&redirect_uri=http%3A%2F%2Flocalhost%2Foauth2callback&response_type=token&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&include_granted_scopes=true&state=pass-through+value

https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive.metadata.readonly&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=http://'+window.location.host+'/share/youtube&response_type=token&client_id=client_id

http://localhost/oauth2callback#state=pass-through+value&access_token=ya29.Glt0BCOlMXzj7C78y_gstOIsHMU8wNQFMjIwTAvfEQPUSGUqeJFNQHAtlmyqNuUDKV0-WVaGP1ZcYVQIVy2oBMNTyw16AbhO7amLnbaJkomu8y-kdAb2thUwcLeG&token_type=Bearer&expires_in=3600&scope=https://www.googleapis.com/auth/drive.metadata.readonly

*/





  });

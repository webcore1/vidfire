'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:TestCtrl
 * @description
 * # TestCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('TestCtrl', [
    '$rootScope',
    '$cookieStore',
    '$log',
    '$timeout',
    '$location',
    'menu',
    function ($rootScope,$cookieStore, $log, $timeout, $location, menu) {
      var vm = this;
      vm.pageTitle = "Onboard";
      $rootScope.pageTitle = "Onboard";
      vm.menuText ="Testing Menu Links";

      vm.isOpen = isOpen;
      vm.toggleOpen = toggleOpen;
      vm.showSubMenu = showSubMenu;
      vm.autoFocusContent = true;
      vm.menu = menu;
      //toggle open on load
      vm.openOnToggle=false;
      
      vm.status = {
        isFirstOpen: true,
        isFirstDisabled: false
      };

      vm.checkPermission = function(permission){

        
           var permissions = $rootScope.permissions[0];

           //console.log('BIGHERE',permissions[permission]);

           //console.log('permissions',permissions);

           if(permissions[permission]===true || permission === "*"){
              //console.log('TRUE',permissions[permission],permission);
              return true;
           }
      
      }

      vm.checkTogglePermission = function(section){

          var permissions = $rootScope.permissions[0];
          var exists;

          //var permissions = $rootScope.permissions[0];


          angular.forEach(section.pages, function(page, key) {

            //console.log('page.permissionId',page.permissionId);

            if(permissions[page.permissionId]===true || page.permissionId === "*") exists = true;

          });

          if(exists) return true;

          //return true;
      
      }


      function isOpen(section) {
      //return true;
        return menu.isSectionSelected(section);
      }
      function toggleOpen(section) {

          menu.toggleSelectSection(section);
          if (vm.openOnToggle === false){
            vm.openOnToggle=true;
          }else{
            vm.openOnToggle=false;
          }
      }
      function showSubMenu() {
        return vm.openOnToggle;
      }
    }]);

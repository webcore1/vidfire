'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:JobsCtrl
 * @description
 * # JobsCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
  .controller('JobsCtrl', function ($scope,api) {

  	$scope.pagetitle = 'Jobs';
  	$scope.icon = 'sitemap';


	api.getAllJobs().
		then(function(response) {
			
			console.log(response);

	    $scope.jobsList = response.data;

	}).catch();



  });

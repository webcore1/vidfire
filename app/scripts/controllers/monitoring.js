'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:OpsjsjqCtrl
 * @description
 * # OpsjsjqCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
    .controller('MonitoringCtrl', function($rootScope, $scope, $location, $mdDialog, $anchorScroll, $timeout) {

        $scope.pagetitle = 'Monitoring';
        $scope.icon = 'eye';

        $scope.state = {
            'streams': {
                "green": 21014,
                "yellow": 34,
                "red": 3

            },

            'batch': {
                "green": 5344,
                "yellow": 2,
                "red": 1

            },

            'ofz': {
                "green": 34543,
                "yellow": 2,
                "red": 3

            },

            'tsz': {
                "green": 80004,
                "yellow": 4,
                "red": 3
            }
        };


        $scope.monitoring = {

            'streams': {
                'headline': 'Ingestion Streams',
                'labels': ['Stream ID', '# of Nodes Used', '# of Msgs Received'],
                'data': [
                    ['YSD', '1', '3023'],
                    ['GOJ', '1', '234'],
                    ['FWEF', '1', '657'],
                    ['F3DD', '3', '34332'],
                    ['HDH', '2', '5544']
                ]
            },
            'batch': {
                'headline': 'Batch Items',
                'labels': ['Source Data', 'Job Execution Time', '# of Files Processed'],
                'data': [
                    ['DEMO-DATA1', '2s', '100'],
                    ['DEMO-DATA2', '1s', '332'],
                    ['DEMO-DATA3', '2s', '321'],
                    ['DEMO-DATA4', '3s', '333'],
                    ['DEMO-DATA5', '1s', '331']
                ]
            },
            'ofz': {
                'headline': 'OFZ',
                'labels': ['Source Data', 'Job Execution Time', '# of Files Processed'],
                'data': {

                    'green': [
                        ['DEMO-DATA1', '2s', '100'],
                        ['DEMO-DATA2', '1s', '332'],
                        ['DEMO-DATA3', '2s', '321'],
                        ['DEMO-DATA4', '3s', '333'],
                        ['DEMO-DATA5', '1s', '331']
                    ],
                    'yellow': [
                        ['DEMO-DATA1', '2s', '100'],
                        ['DEMO-DATA3', '1s', '332'],
                        ['DEMO-DATA2', '2s', '321'],
                        ['DEMO-DATA1', '3s', '333'],
                        ['DEMO-DATA5', '1s', '331']
                    ],
                    'red': [
                        ['DEMO-DATA1', '2s', '100'],
                        ['DEMO-DATA2', '1s', '332'],
                        ['DEMO-DATA2', '2s', '321'],
                        ['DEMO-DATA0', '3s', '333'],
                        ['DEMO-DATA1', '1s', '331']
                    ]
                }
            },
            'tsz': {
                'headline': 'OFZ',
                'labels': ['Source Data', 'Job Execution Time', '# of Files Processed'],
                'data': {

                    'green': [
                        ['DEMO-DATA1', '2s', '100'],
                        ['DEMO-DATA2', '1s', '332'],
                        ['DEMO-DATA3', '2s', '321'],
                        ['DEMO-DATA4', '3s', '333'],
                        ['DEMO-DATA5', '1s', '331']
                    ],
                    'yellow': [
                        ['DEMO-DATA1', '2s', '100'],
                        ['DEMO-DATA3', '1s', '332'],
                        ['DEMO-DATA2', '2s', '321'],
                        ['DEMO-DATA1', '3s', '333'],
                        ['DEMO-DATA5', '1s', '331']
                    ],
                    'red': [
                        ['DEMO-DATA1', '2s', '100'],
                        ['DEMO-DATA2', '1s', '332'],
                        ['DEMO-DATA2', '2s', '321'],
                        ['DEMO-DATA0', '3s', '333'],
                        ['DEMO-DATA1', '1s', '331']
                    ]
                }
            }

        }




        $scope.showOperationsDialog = function(ev, info, msgStatus) {

            if (msgStatus === 'single') {
                $scope.monitoring[info]['data']['single'] = $scope.monitoring[info]['data'];
            }

            $scope.popData = $scope.monitoring[info];

            console.log('msgStatusData', $scope.msgStatusData);

            $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'views/operations/streams.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: true,
                    locals: {
                        popData: $scope.popData,
                        msgStatus: msgStatus
                    }
                })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        function DialogController($scope, $mdDialog, popData, msgStatus) {

            $scope.popData = popData;
            $scope.msgStatus = msgStatus;

            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }

    });
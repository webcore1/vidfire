'use strict';
/**
 * @ngdoc function
 * @name uiApp.controller:DataflowCtrl
 * @description
 * # DataflowCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
    .controller('CreateCtrl', function($window, $scope, $rootScope, $sce, $route, $routeParams, $http, $location, api) {

        var create = this;
        create.model = {};

        $scope.pagetitle = 'Burn a Video';
        $scope.icon = 'upload';
        $scope.templates = [];
        $scope.thisTemplate;
        $scope.thisJob;
        $scope.nextSce;
        $scope.totalSces;


        var user = $rootScope.globals.currentUser.username;
        var vidId = $routeParams.vidId;
        var sceNum = parseInt($routeParams.sceNum);
        var jobId = $routeParams.jobId;


    create.fields = [ 

        {
            className: 'display-flex',
            hideExpression: "$scope.nextSce != null",
            fieldGroup: [


                {
                    className: 'flex-4',
                    key: "title",
                    type: 'input',
                    templateOptions: {
                        type: 'text',
                        label: 'Video Title',
                        placeholder: 'ENTER A VIDEO TITLE',
                        required: true
                    },

                },

            ]
        },

    ];

        //after done editing video, send to start/check status
        if(sceNum===0){
            $location.path('/status/'+jobId+'/');
        }
       
        if(!vidId && !jobId){

            api.all().
            then(function(response) {

                $scope.templates = response.data;

                templatesGrid();




            }).catch();


        }else if(vidId && !jobId){

            getTemplate();

        }else if(vidId && jobId){

            getTemplate();
            getJob();

        }


function getTemplate(){

    api.one(vidId).
    then(function(response) {

        $scope.thisTemplate = response.data;

        console.log('$scope.thisTemplate',$scope.thisTemplate);

    }).catch();

}

function getJob(){

    api.getJob(jobId).
    then(function(response) {

        $scope.thisJob = response.data;

        console.log('$scope.thisJob',$scope.thisJob);

        scene();

    }).catch();

}

function scene(){

    $scope.totalSces = Object.keys($scope.thisJob.template.payload.scenes).length;

    if(!sceNum){
        $scope.nextSce = 1;
    }else if(sceNum === $scope.totalSces){
        $scope.nextSce = 0;
    }else{
        $scope.nextSce = sceNum + 1;
    }

    buildForm();
}


function buildForm(){

    var items;
    var fields = [];

    if(sceNum){

        var textFields = genTextFields($scope.thisJob.template.payload.scenes[sceNum].texts);
        var mediaFields = genMediaFields($scope.thisJob.template.payload.scenes[sceNum].media);

    }else{

        var textFields = genTextFields($scope.thisJob.template.payload.constants.texts);
        var mediaFields = genMediaFields($scope.thisJob.template.payload.constants.media);

    }

    fields = textFields.concat(mediaFields);

    console.log('fields',fields);

    create.fields = fields;

}


function genTextFields(items){

   var newFields = [];

    angular.forEach(items, function(item, iKey) {


        if(item.rows === 1){

            var itemType = "input";

        }else{

            var itemType = "textarea";

        }


        this.push({
                    className: 'display-flex',
                    fieldGroup:[{
                        className: 'flex-4',
                        key: iKey,
                        type: itemType,
                        defaultValue: item.input,
                        templateOptions: {
                            label: item.desc,
                            placeholder: '',
                            required: true,
                            rows: item.rows
                        }
                    }]
                })

    },newFields);

    return newFields;

}


function genMediaFields(items){

   var newFields = [];

    angular.forEach(items, function(item, iKey) {


        if(iKey === "MUSIC"){

            
        }

        if(item.type === "logo"){


        }


        this.push({
                    className: 'display-flex',
                    fieldGroup:[{
                        className: 'flex-4',
                        key: iKey,
                        type: 'inputImageURI',
                        defaultValue: item.uri,
                        templateOptions: {
                            label: item.desc,
                            placeholder: '',
                            required: true,
                        }
                    }]
                })

    },newFields);


    console.log('genMediaFields',newFields);

    return newFields;

}

function templatesGrid(){




    $scope.config = {


/*

        tracks: [
            {
                src: "http://www.videogular.com/assets/subs/pale-blue-dot.vtt",
                kind: "subtitles",
                srclang: "en",
                label: "English",
                default: ""
            }
        ],

        */

        theme: "bower_components/videogular-themes-default/videogular.css",

        plugins: {

            poster: "http://www.videogular.com/assets/images/videogular.png"

        }
    }

}


    create.originalFields = angular.copy(create.fields);

    create.onSubmit = onSubmit;
    create.options = {};

    // function definition
    function onSubmit() {

        if(vidId && !jobId){

            createJob();

        }else if (vidId && jobId){

            saveJob();

        }

        /*if($scope.nextSce !== 0){

            $location.path('/create/'+vidId+'/'+$scope.nextSce);

        }*/

        //alert('submit');

        //create.loading = true;

        //create.options.updateInitialValue();
        //alert('added');
        //toast.fire('Source Onboarding started - Check Job Status','reg');
        //onboardservice.saveOnboardSource(create.model);
        //create.saveOnboardSource();
        //create.model = {};
        //create.options.resetModel();
    }


function createJob(){

    var pl = {
        "title": create.model.title,
        "owner": user,
        "status": "new",
        "date_created": Date(),
        "date_modified": Date(),
        "template": {
          "id": vidId,
          "payload": $scope.thisTemplate.template
        }
      }

      //can't set jobId here, it hasn't received one yet from
      

      console.log(pl);

        api.newJob(pl).
        then(function(res) {

        var jobId = res.data.id;

        $location.path('/burn/'+vidId+'/'+jobId+'/');

    });

}




function saveJob(){

    //console.log(create.model);

    $scope.thisJob.template.payload.constants.texts = jobInputs($scope.thisJob.template.payload.constants.texts);
    $scope.thisJob.template.payload.constants.media = jobInputsMedia($scope.thisJob.template.payload.constants.media);

     angular.forEach($scope.thisJob.template.payload.scenes, function(item, iKey) {

        $scope.thisJob.template.payload.scenes[iKey].texts = jobInputs($scope.thisJob.template.payload.scenes[iKey].texts);
        $scope.thisJob.template.payload.scenes[iKey].media = jobInputsMedia($scope.thisJob.template.payload.scenes[iKey].media);

    })

    $scope.thisJob.date_modified = Date();

    console.log($scope.thisJob);

    api.putJob($scope.thisJob).
    then(function(res) {

        if(res.status === 200){

            $location.path('/burn/'+vidId+'/'+jobId+'/'+$scope.nextSce);

        }

        //$location.path('/create/'+vidId+'/'+jobId+'/');

    });

}


function jobInputs(items){

    angular.forEach(items, function(item, iKey) {

        if(iKey in create.model){

            item.input = create.model[iKey];

        }

    });

    return items;

}



function jobInputsMedia(items){

    angular.forEach(items, function(item, iKey) {

        if(iKey in create.model){

            item.input = create.model[iKey];
            item.uri = create.model[iKey];

        }

    });

    return items;

}


});

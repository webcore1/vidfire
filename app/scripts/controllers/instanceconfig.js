'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:InstanceconfigCtrl
 * @description
 * # InstanceconfigCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
    .controller('InstanceconfigCtrl', function($scope, $rootScope, $route, $routeParams, $http, $location, instancesData, $mdToast, toast) {

        $scope.pagetitle = 'Configuration';
        $scope.icon = 'wrench';
        $scope.instanceName = "";
        $scope.instanceId = $routeParams.instanceId;
        $scope.configId = $routeParams.configId;
        $scope.profileId = $routeParams.profileId;
        $scope.instanceConfigs;
        $scope.instanceConfigsTable = false;
        $scope.configForm = false;
        $scope.configType = "";
        $scope.configPro = "";
        $scope.path = $location.$$path;
        $scope.bigError ="";
        $scope.post = true;
        $scope.editing = "";



        function catchError (reply) { 

        var message = "";

        if (reply.status === -1) {

            message = "No Internet Connection!";

        }else if(reply.status === 404){

            message ="Looks like the back-end is down. Contact your Administrator!";


        }else{

            message = reply.data.meta.errorMessage;


        }  

        console.log('failed-catch',reply);

        toast.fire(message,'error');

        };



        //get instance info
        instancesData.fetch($scope.instanceId).then(function(data) {

            //console.log('datea', data.data.ip);

            $scope.instanceName = data.data.name;
            var eurekaServer = data.data.ip;

            //vm.originalFields = angular.copy(vm.fields);

        }).catch(catchError);



        /*if in edit mode*/
        if($scope.configId){


            //POST false put it in PUT mode.
            $scope.post = false;
            $scope.configType = $scope.configId;
            $scope.configPro = $scope.profileId;

            console.log('edit mode', $scope.configType);

            instancesData.fetchConfig($scope.configType,$scope.configPro).then(function(reply) {
               
            //alert('here');

                $scope.editing = true;
                buildConfigForm(reply);

            }).catch(catchError);

        }


        //for fetching all configs available for the instance
        instancesData.fetchInstanceConfigs().then(function(reply) {


            var data = reply.data.data;
            var meta = reply.data.meta;

            if (reply.status === 200) {


                if (Object.keys(data).length !== 0) {

                    $scope.instanceConfigs = data;
                    $scope.instanceConfigsTable = true;

                    console.log('data', data);

                }

            }

        }).catch(catchError);




        function dashFixBack(prop) {
            prop = prop.split("_").join("-");
            //console.log(prop);
            return prop;
        }

        function dashFix(prop) {
            prop = prop.split("-").join("_");
            //console.log(prop);
            return prop;
        }

        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

        function convert(fields) {
            var copy = fields;
            var formFields = [];

            angular.forEach(copy, function(val, prop, test) {

                console.log('val', val);


                var inputType = "input";
                var fieldType = "input";

                //if 

                if($scope.editing){

                    var defaultValue = val.values.property_value;

                }else{

                    var defaultValue = val.values.default_value;

                }


                //if config

                //check if value is a number
                if (val.values.type === "INTEGER") {

                    //console.log('number:' , val.values.default_value);
                    inputType = "number";
                    defaultValue = parseInt(defaultValue);

                } else if (typeof(val.values.default_value) === "boolean") {

                    fieldType = "mdSwitch";
                    //console.log(prop, 'its true or false');

                } else if (Object.keys(val.values.default_value).length > 150) {

                    fieldType = "mdtextArea";

                };

                var field = {
                    'key': dashFix(val.values.property_name),
                    'type': fieldType,
                    'className': '',
                    'defaultValue': defaultValue,
                    'templateOptions': {
                        'label': val.values.property_name,
                        'type': inputType,
                        /*, required: true */
                        'required': val.values.required,
                        'pattern': val.values.validation_regex,
                        'patternValidationMessage': 'Need description of errors!'

                    }
                };

                formFields.push(field);

            });
            return formFields;
        }


        var vm = this;
        vm.onSubmit = onSubmit;
        vm.model = {};
        vm.options = {};

        // function definition

        function objToString(obj) {
            var objsCol = [];
            for (var p in obj) {
                if (obj.hasOwnProperty(p)) {
                    //str += p + '::' + obj[p] + '\n';
                    console.log(p, '::', obj[p]);
                } else {
                    console.log('no more property stop, add result to objsCol');
                }
            }
            return str;
        }



        function onSubmit() {

            vm.options.updateInitialValue();

            var fields = recursify(vm.model); // convert result to JSON
            var data = [];
            var field;

            console.log('result', fields);

            angular.forEach(fields, function(val, prop, test) {

                console.log(val, prop, test);

                field = {
                    "typeName": "runtimeConfig",
                    "values": {
                        "configuration_unit_name": "cie",
                        "property_name": dashFixBack(prop),
                        "property_value": val
                    }
                };

                data.push(field);

            })
            

            var postData = { "data": data };


            console.log("string",JSON.stringify(postData,replacer,4));


                console.log('configType',$scope.configType );


                instancesData.configSAVE($scope.post,$scope.configType,$scope.configPro,JSON.stringify(postData,replacer,4)).then(function(reply) {
                    
                    toast.fire("Config was saved!",'success');

                }).catch(catchError);



        }



        vm.newConfigSubmit = newConfigSubmit;
        vm.newConfigModel = {};
        vm.newConfigOptions = {};

        vm.newConfigFields =

            [

                {
                    className: 'display-flex',
                    fieldGroup:


                        [

                        {
                            className: 'flex-1',
                            key: "configTemplateType",
                            type: 'select',

                            templateOptions:

                            {
                                label: 'Select configuration type',
                                theme: "custom",
                                multiple: false,
                                labelProp: "configuration_unit_name",
                                valueProp: "configuration_unit_name",
                                required: true,
                                "options": []
                            },

                            controller: /* @ngInject */ function($scope, instancesData) {
                                $scope.to.loading = instancesData.fetchConfigTemplatesAll().then(function(response) {



                                    var responseMod = response.data.data;

                                    responseMod.forEach(function(obj, id) {
                                        responseMod[id].configuration_unit_name = obj.values.configuration_unit_name;
                                        //responseMod[id].id = id;
                                    })

                                    console.log('responseMod', responseMod);

                                    $scope.to.options = responseMod;

                                    // note, the line above is shorthand for:
                                    // $scope.options.templateOptions.options = data;
                                    return response;
                                });
                            }


                        },

                        {

                            className: 'flex-3',
                            key: "configTemplateName",
                            type: 'input',

                            templateOptions:

                            {
                                type: 'text',
                                label: 'Give your configuration a unique name',
                                placeholder: 'CONFIGURATION NAME',
                                required: true,
                                pattern: '[A-z]*',
                                patternValidationMessage: 'Name cannot have spaces, special characters or spaces!'


                            }
                        }

                    ]
                }

            ];



        function buildConfigForm(reply){


            if (reply.status === 200) {

                var data = reply.data.data;

                if (Object.keys(data).length !== 0) {

                    //$scope.instanceName = reply.data.name;
                    //console.log('reply',data);

                    vm.fields = convert(data);
                    vm.originalFields = angular.copy(vm.fields);
                    //$scope.instanceConfigsTable = true;

                    //console.log('data',data);
                    $scope.configForm = true;


                    if($scope.post){

                        $scope.configType = vm.newConfigModel.configTemplateType;
                        $scope.configPro = vm.newConfigModel.configTemplateName;


                    };

                    console.log('testing',$scope.configPro);



                }

            } 


        }



        function newConfigSubmit() {
            vm.newConfigOptions.updateInitialValue();
            //alert(vm.newConfigModel.configTemplateName);
            instancesData.fetchTemplate(vm.newConfigModel.configTemplateType).then(function(reply) {
                

            var data = reply.data.data;
            var meta = reply.data.meta;

            if (reply.status === 200) {

                  toast.fire("Default configs fetched, edit per your specification and save.",'success');

                $scope.editing = false;
                 buildConfigForm(reply);

            } else {

                alert(meta.errorMessage);

            }

            }).catch(catchError);
        }
    }).directive('configProfile', function () {


  return {

 restrict: 'E',
    scope: {
      profiles: '=',
      path: '@path',
      config: '@config'
    },


    template: '<span  ng-repeat="profile in profiles">'+
                '<a ng-href="/#{{ path }}/{{config}}/{{profile}}">'+
                '<button type="button" class="md-button md-ink-ripple" ng-hide="row.editable" aria-hidden="false">{{ profile }}</button>'+
                '</a>'+
                '</span>',
                //'</ul>',

   link: function(scope, element, attrs){

        //attrs.$observe('path', function(value) {

            console.log(scope);

          //});

    }
  }
})
'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:SecurityCtrl
 * @description
 * # SecurityCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')


.controller('SecurityCtrl', function($scope, oiSelect, $filter, securityService ) {

    $scope.pagetitle = 'Security';
    $scope.icon = 'shield';


    function catchError (reply) { 

    var message = "";

    if (reply.status === -1) {

        message = "No Internet Connection!";

    }else if(reply.status === 404){

        message ="Looks like the back-end is down. Contact your Administrator!";

    }else{

        message = reply.data.meta.errorMessage;

    }  

    console.log('failed-catch',reply);

    toast.fire(message,'error');

    };


    $scope.version = oiSelect.version.full;
    //$scope.shopArr = ShopArr.query();


    $scope.metrixs = [{
        id: 1,
        name: 'Secret'
    }, {
        id: 2,
        name: 'Confidential'
    }, {
        id: 3,
        name: 'Internal'
    }, ];

    $scope.selectedMetrix = 'Secret';
    $scope.selectedMetrix2 = 'Internal';


securityService.allPermissions().then(function(data) {


    $scope.permissions = data;

    //console.log('allGRoles', angular.toJson(data));
    console.log('allPermissions', data);


}).catch(catchError);


/*
securityService.allGroups().then(function(data) {
    $scope.securityGroups = data;
    //console.log('allGRoles', angular.toJson(data));
    console.log('securityGroups', data);
}).catch(catchError);
*/


securityService.allGRoles().then(function(data) {


    $scope.Roles = data;
    $scope.countRules = $scope.Roles.length;

    //console.log('allGRoles', angular.toJson(data));
    console.log('allGRoles', data);


    //vm.originalFields = angular.copy(vm.fields);

}).catch(catchError);


    $scope.addSecurityRulesRow = function() {
        $scope.securityRules.push({
            'id': $scope.id,
            'role': $scope.role,
            'Lorem': $scope.Lorem,
            'ipsum': $scope.ipsum,
            'incididunt': $scope.incididunt,
            'commodo': $scope.commodo,
            'Duis': $scope.Duis
        });
        $scope.id = '';
        $scope.role = '';
        $scope.Lorem = '';
        $scope.ipsum = '';
        $scope.incididunt = '';
        $scope.commodo = '';
        $scope.Duis = '';
    };

    $scope.addSecurityRulesRowInline = function() {
        $scope.securityRules.push({
            'id': 7,
            'role': undefined,
            'Lorem': false,
            'ipsum': false,
            'incididunt': false,
            'commodo': false,
            'Duis': false
        });
    };


    $scope.entity = {};


    $scope.clickedGroup = function(role){
     
        console.log('role',angular.toJson(role));

        /*angular.forEach(role.groups, function(val, prop) {

            groupsList.push(val.id);

        })

        var update = */

        
        securityService.updateGRole(role).then(function(data) {
            console.log(data);
        })


    }

    $scope.clickedPermission = function(permi){
     
        console.log(angular.toJson(permi));

        /*angular.forEach(Permi.groups, function(val, prop) {

            groupsList.push(val.id);

        })

        var update = */

        
        securityService.updateGRole(permi).then(function(data) {
            console.log(data);
        })


    }

    $scope.editRow = function(index) {
        $scope.entity = $scope.Roles[index];
        $scope.entity.index = index;
        $scope.entity.editable = true;
    };

    $scope.removeRow = function(index) {
        //$scope.Roles.splice(index, 1);
        //var index = index -1;
        console.log($scope.Roles[index].id);
        

        securityService.destroyGRole($scope.Roles[index].id).then(function(data) {
            console.log(data);
            $scope.Roles.splice(index, 1);
        })



    };

    $scope.saveRow = function(index) {
        $scope.Roles[index].editable = false;
        console.log(angular.toJson($scope.Roles[index]));

        securityService.updateGRole($scope.Roles[index]).then(function(data) {
            console.log(data);
            if(data.id){
                //$scope.Roles[index].id = data.id;
                console.log($scope.Roles);

            }
        })

    };

    $scope.addRow = function() {

        console.log($scope.Roles);

        var newRole = {
            role: "",
            groups: [],
            editable: true
        };

        $scope.Roles.push(newRole);


        securityService.postGRole(newRole).then(function(data) {
            console.log(data);
            if(data.id){
                var index = $scope.Roles.length - 1;
                $scope.Roles[index].id = data.id;
                console.log($scope.Roles);

            }
        })


        //$scope.countRules = $scope.countRules + 1;
        //$scope.Roles[$scope.countRules].editable = true;


    };


    $scope.securityTasks = ["Role 1", "Role 2", "Role 3", "Role 4", "Role 5"];


    /* table with permissions*/


    $scope.newSecurityRule = {};
    $scope.newRuleTask = {};
    $scope.show = false;
    $scope.newTask = "";
    $scope.addSecurityRowInline = function() {
        $scope.show = true;
        Object.keys($scope.securityTasks).forEach(function(key) {
            $scope.newRuleTask[key] = false;
        })
    };
    $scope.addSecurityRulesRow = function() {
        if (Object.keys($scope.newSecurityRule).length > 0) {
            $scope.newSecurityRule.tasks = $scope.newRuleTask;

            $scope.Roles.push($scope.newSecurityRule);
            $scope.show = false;
            $scope.newSecurityRule = {};
            $scope.newRuleTask = {};
        }

    };

    $scope.saveTask = function() {
        if ($scope.newTask !== "" && $scope.newTask) {
            $scope.securityTasks[$scope.tasksLen] = $scope.newTask;
            $scope.addTaskCheck = false;
        } else {
            $scope.Roles.forEach(function(rule) {
                delete rule.tasks[$scope.tasksLen];
            });
        }
        $scope.newTask = "";

    };

    $scope.addTask = function() {
        $scope.addTaskCheck = true;
        $scope.tasksLen = String(Object.keys($scope.securityTasks).length + 1);
        $scope.Roles.forEach(function(rule) {
            rule.tasks[$scope.tasksLen] = false;
        });
    };

    $scope.addTaskCheck = false;
    
});



/*
    $scope.Roles = [{
        "id": 1,
        "role": "admin 1",
        "groups": [1, 3, 4, 5],
        "tasks": {
            1: false,
            2: true,
            3: false,
            4: false,
            5: true
        },
        "editable": false
    }, {
        "id": 2,
        "role": "admin 2",
        "groups": [1, 3, 4, 5],
        "tasks": {
            1: false,
            2: true,
            3: false,
            4: false,
            5: true
        },
        "editable": false
    }, {
        "id": 3,
        "role": "admin 3",
        "groups": [1, 3, 4, 5],
        "tasks": {
            1: false,
            2: true,
            3: true,
            4: false,
            5: true
        },
        "editable": false
    }];
*/

'use strict';

/**
 * @ngdoc function
 * @name uiApp.controller:NavctrlCtrl
 * @description
 * # NavctrlCtrl
 * Controller of the uiApp
 */
angular.module('uiApp')
    .config(function($mdIconProvider) {
        $mdIconProvider
            //.iconSet("call", 'img/icons/sets/communication-icons.svg', 24)
            //.iconSet("social", 'img/icons/sets/social-icons.svg', 24)
            //.iconSet("navigation", 'img/icons/sets/core-icons.svg', 24);
    })
    .controller('navCtrl', function DemoCtrl($rootScope,$mdDialog, $scope, $location) {
        var originatorEv;

        $scope.currentUser = $rootScope.globals.currentUser.username;


        this.go = function(path) {
            $location.path(path);
        };

        this.openMenu = function($mdOpenMenu, ev) {
            //log('openMenu',ev);
            originatorEv = ev;
            $mdOpenMenu(ev);
        };

        this.notificationsEnabled = true;
        this.toggleNotifications = function() {
            this.notificationsEnabled = !this.notificationsEnabled;
        };

        /*
            this.redial = function() {
              $mdDialog.show(
                $mdDialog.alert()
                  .targetEvent(originatorEv)
                  .clickOutsideToClose(true)
                  .parent('body')
                  .title('Suddenly, a redial')
                  .textContent('You just called a friend; who told you the most amazing story. Have a cookie!')
                  .ok('That was easy')
              );

              originatorEv = null;
            };
        */

        //this.checkVoicemail = function() {
        // This never happens.
        //};
    });
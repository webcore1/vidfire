'use strict';

/**
 * @ngdoc directive
 * @name uiApp.directive:opStatus
 * @description
 * # opStatus
 */
angular.module('uiApp')

.directive('opStatus', function() {

    return {

        restrict: 'E',
        scope: {
            info: '@info'
        },

        templateUrl: function(elem, attrs) {
            return 'views/monitoring/' + attrs.view + '.html';
        }

        //templateUrl: 'views/monitoring/'+view+'.html'
    };


});
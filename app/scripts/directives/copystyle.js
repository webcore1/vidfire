'use strict';

/**
 * @ngdoc directive
 * @name uiApp.directive:copystyle
 * @description
 * # copystyle
 */
angular.module('uiApp')
.directive('copyStyle', function ()  {
    function myCustomStyle($scope, element, attrs) {
          $scope.customStyle = {
        height:476+'px'
              //height:element[0].offsetHeight+'px'
        };
    }
      return {
      restrict: 'AE',
      link: myCustomStyle
      };
  });
  // .directive('copystyle', function () {
  //   return {
  //     template: '<div></div>',
  //     restrict: 'E',
  //     link: function postLink(scope, element, attrs) {
  //       element.text('this is the copystyle directive');
  //     }
  //   };
  // });

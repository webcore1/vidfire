/**
 * Created by Liu on 12/20/2016.
 */
(function() {
    'use strict';


    angular
        .module('uiApp')
        //.run(formConf)

    .run(function(formlyConfig, formlyValidationMessages, formlyApiCheck) {


        formlyConfig.setType({
            name: 'input',
            template: '<input ng-model="model[options.key]" md-maxlength="{{to.maxlength}}" value="{{to.value}}" md-minlength="{{to.minlength}}"> <div class="my-messages" ng-messages="fc.$error" ng-if="options.formControl.$touched"> <div class="some-message" ng-message="{{::name}}" ng-repeat="(name, message) in ::options.validation.messages"> {{message(fc.$viewValue, fc.$modelValue, this)}} </div></div>'
        });


        formlyConfig.setType({
            name: 'inputImageURI',
            template: '<input ng-model="model[options.key]" md-maxlength="{{to.maxlength}}" value="{{to.value}}" md-minlength="{{to.minlength}}"> <div class="my-messages" ng-messages="fc.$error" ng-if="options.formControl.$touched"> <div class="some-message" ng-message="{{::name}}" ng-repeat="(name, message) in ::options.validation.messages"> {{message(fc.$viewValue, fc.$modelValue, this)}} </div></div> <img class="previewImg" src="{{model[options.key]}}">'
        });


        formlyConfig.setType({
            name: 'checkbox',
            template: '<md-checkbox ng-model="model[options.key]">{{to.label}}</md-checkbox>'
        });

        formlyConfig.setType({
            name: 'mdSelect',
            template: '<md-select ng-model="model[options.key]" aria-label="{{to.label}}"><md-option ng-repeat="option in to.options track by $index" >{{option}}</md-option></md-select>'
        });

        formlyConfig.setType({
            name: 'mdSwitch',
            template: '<md-switch ng-model="model[options.key]" aria-label="{{to.label}}"><label>{{to.label}}</label> {{ data.cb1 }}</md-switch><br>'
        });

        formlyConfig.setType({
            name: 'mdtextArea',
            template: '<md-input-container class="md-block"><label>{{to.label}}</label><textarea ng-model="model[options.key]" md-maxlength="{{to.maxlength}}" rows="5" md-select-on-focus=""></textarea></md-input-container>'
        });

        formlyConfig.setType({
            name: 'icon',
            template: '<i class="fa {{to.label}}" aria-hidden="true" style="text-align: center;display: block;margin-top: 30px;font-size: 26px;color: #3fa9f5;"></i>'
        });


        formlyConfig.setType({
            name: 'fieldGroupTitle',
            template: '{{to.label}}'
        });

        formlyConfig.setWrapper({
            name: 'mdLabel',
            types: ['input','inputImageURI'],
            template: '<label>{{to.label}}</label><formly-transclude></formly-transclude>'
        });

        formlyConfig.setWrapper({
            name: 'mdInputContainer',
            types: ['input', 'checkbox','inputImageURI'],
            template: '<md-input-container class="md-block"><formly-transclude></formly-transclude></md-input-container>'
        });




        formlyValidationMessages.addStringMessage('required', 'This field is required!');
        formlyValidationMessages.addStringMessage('number', 'This field is numerical!');
        formlyValidationMessages.addTemplateOptionValueMessage('pattern', 'patternValidationMessage', '', '', 'Invalid Input');

        /**formlyConfig.setType({
          name: 'customInput',
          extends: 'input',
          apiCheck: function(check) {
            return {
              templateOptions: {
                foo: check.string.optional
              }
            };
          }
        });**/
    });



})();
'use strict';

/**
 * @ngdoc service
 * @name uiApp.templateService
 * @description
 * # templateService
 * Service in the uiApp.
 */
angular.module('uiApp')
  .service('api', function ($http,ENV) {

    var service = this;

	service.all = function () {
	  return $http.get(ENV.ENDPOINT_URI+'/api/templates');
	};

	service.one = function (id) {
	  return $http.get(ENV.ENDPOINT_URI+'/api/templates/'+id);
	};

	service.delete = function (id) {
	  return $http.delete(ENV.ENDPOINT_URI+'/api/templates/'+id);
	};

	service.newJob = function (pl) {
	  return $http.post(ENV.ENDPOINT_URI+'/api/jobs/',pl);
	};

	service.getJob = function (id) {
	  return $http.get(ENV.ENDPOINT_URI+'/api/jobs/'+id);
	};

	service.getAllJobs = function () {
	  return $http.get(ENV.ENDPOINT_URI+'/api/jobs/');
	};

	service.putJob = function (pl) {
	  return $http.put(ENV.ENDPOINT_URI+'/api/jobs/',pl);
	};

	service.putTemplate = function (id,pay) {
	  return $http.put(ENV.ENDPOINT_URI+'/api/templates/'+id,pay);
	};

	service.getAllvideos = function () {
	  return $http.get(ENV.ENDPOINT_URI+'/api/videos/');
	};


  });

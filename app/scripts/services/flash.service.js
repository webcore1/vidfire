'use strict';

/**
 * @ngdoc service
 * @name uiApp.flash.service
 * @description
 * # flash.service
 * Service in the uiApp.
 */
angular.module('uiApp')
    .factory('FlashService', FlashService);

FlashService.$inject = ['$rootScope','$mdToast'];

function FlashService($rootScope,$mdToast) {

    var service = {};

    service.Success = Success;
    service.Error = Error;

    initService();

    return service;

    function initService() {
        $rootScope.$on('$locationChangeStart', function() {
            clearFlashMessage();
        });

        function clearFlashMessage() {
            var flash = $rootScope.flash;
            if (flash) {
                if (!flash.keepAfterLocationChange) {
                    delete $rootScope.flash;
                } else {
                    // only keep for a single location change
                    flash.keepAfterLocationChange = false;
                }
            }
        }
    }

    function Success(message, keepAfterLocationChange) {

        $rootScope.flash = {
            message: message,
            type: 'success',
            keepAfterLocationChange: keepAfterLocationChange
        };

        fire(message,$rootScope.flash.type);
    }

    function Error(message, keepAfterLocationChange) {

        //alert('test');

        $rootScope.flash = {
            message: message,
            type: 'error',
            keepAfterLocationChange: keepAfterLocationChange
        };

        fire(message,$rootScope.flash.type);
    }


    function fire(message,type){

        $mdToast.show($mdToast
            // ng-scope _md md-default-theme md-top md-right
            .simple() 
            .toastClass(type)
            .hideDelay('5000')
            .position('top right')
            .textContent(message)
            .action('OK')
            .highlightAction(true)
            //.highlightClass('md-warn')

            );        
    }



}
'use strict';

/**
 * @ngdoc service
 * @name uiApp.user.service
 * @description
 * # user.service
 * Service in the uiApp.
 */

angular.module('uiApp')
  .factory('UserService', UserService);
 
    UserService.$inject = ['$http','ENV'];

    function UserService($http,ENV) {
        var service = {};
 
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Login = Login;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.Details = Details;
        service.Permissions = Permissions;


 
        return service;
 
        function GetAll() {
            return $http.get(ENV.ENDPOINT_URI+'/api/vfUsers').then(handleSuccess, handleError('Error getting all users'));
        }
 
        function GetById(id) {
            return $http.get(ENV.ENDPOINT_URI+'/api/vfUsers/' + id).then(handleSuccess, handleError('Error getting user by id'));
        }
 

        function Login(user){
            return $http.post(ENV.ENDPOINT_URI+'/api/vfUsers/login', user).then(handleSuccess, handleError('Wrong username and or password!'));
        }

        function Details(user){
            return $http.get(ENV.ENDPOINT_URI+'/api/vfUsers/'+user.userId+'?access_token='+user.id).then(handleSuccess, handleError('Error getting user info'));
        }

        function Permissions(user,group){
            return $http.get(ENV.ENDPOINT_URI+'/api/GRoles/'+group+'?access_token='+user.id).then(handleSuccess, handleError('Error getting user info'));
        }

 
        function Create(user) {
            return $http.post(ENV.ENDPOINT_URI+'/api/vfUsers', user).then(handleSuccess);
        }
 
        function Update(user) {
            return $http.put(ENV.ENDPOINT_URI+'/api/vfUsers/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
        }
 
        function Delete(id) {
            return $http.delete(ENV.ENDPOINT_URI+'/api/vfUsers/' + id).then(handleSuccess, handleError('Error deleting user'));
        }
 
        // private functions
 
        function handleSuccess(res) {
            return res.data;
        }
 
        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

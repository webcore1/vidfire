'use strict';

/**
 * @ngdoc service
 * @name uiApp.toast
 * @description
 * # toast
 * Service in the uiApp.
 */
angular.module('uiApp')
  .service('toast', function ($mdToast) {

	var service = this;

	service.fire = function(message,type){

        $mdToast.show($mdToast
            // ng-scope _md md-default-theme md-top md-right
            .simple() 
            .toastClass(type)
            .hideDelay('10000')
            //.position('bottom right')
            .textContent(message)
            .action('OK')
            .highlightAction(true)
            //.highlightClass('md-warn')

            );        
    }

    // AngularJS will instantiate a singleton by calling "new" on this function
  });

'use strict';

/**
 * @ngdoc service
 * @name uiApp.security.service
 * @description
 * # security.service
 * Service in the uiApp.
 */
angular.module('uiApp')
  .service('securityService', function ($http,ENV) {

        var service = this;

	function getUrl(path) {
	    return ENV.ENDPOINT_URI + "/api" + path;
	}

    service.allPermissions = function() {
         return $http.get(getUrl('/Permissions')).then(handleSuccess, handleError('Error getting Permissions'));
    };

    service.allGroups = function() {
         return $http.get(getUrl('/Groups')).then(handleSuccess, handleError('Error getting allGroups'));
    };

    service.allGRoles = function() {
         return $http.get(getUrl('/GRoles')).then(handleSuccess, handleError('Error getting allGRoles'));

    };

    service.allInstances = function() {
         return $http.get(getUrl('/Instances')).then(handleSuccess, handleError('Error getting allGRoles'));
    };


    service.updateGRole = function(item) {
         return $http.put(getUrl('/GRoles'),item).then(handleSuccess, handleError('Error updating roles'));
    };

    service.postGRole = function(item) {
         return $http.post(getUrl('/GRoles'),item).then(handleSuccess, handleError('Error updating roles'));
    };


    service.updateInstances = function(item) {
         return $http.put(getUrl('/Instances'),item).then(handleSuccess, handleError('Error updating Instances'));
    };


    service.destroyGRole = function(item) {
         return $http.delete(getUrl('/GRoles/'+item)).then(handleSuccess, handleError('Error getting all users'));
    };


    // private functions

    function handleSuccess(res) {
        return res.data;
    }

    function handleError(error) {
        return function () {
            return { success: false, message: error };
        };
    }



  });

(function() {

    'use strict';

    angular.module('common.services')
        .factory('menu', [
            '$location',
            '$rootScope',
            function($location) {

                var sections = [{
                    name: 'Getting Started',
                    state: 'home.gettingstarted',
                    type: 'link'
                }];

                sections.push({
                    name: 'Ingestion',
                    type: 'toggle',
                    pages: [{
                        name: '<i class="fa fa-eye" aria-hidden="true"></i> Metadata',
                        type: 'link',
                        state: 'home.ingestion.metadata',
                        icon: 'fa fa-group'
                    }, {
                        name: 'Onboard',
                        state: 'home.ingestion.onboard',
                        type: 'link',
                        icon: 'fa fa-map-marker'
                    }, {
                        name: 'Ingest',
                        state: 'home.ingestion.ingest',
                        type: 'link',
                        icon: 'fa fa-plus'
                    }]
                });

                sections.push({
                    name: 'Munchies',
                    type: 'toggle',
                    pages: [{
                        name: 'Cheetos',
                        type: 'link',
                        state: 'munchies.cheetos',
                        icon: 'fa fa-group'
                    }, {
                        name: 'Banana Chips',
                        state: 'munchies.bananachips',
                        type: 'link',
                        icon: 'fa fa-map-marker'
                    }, {
                        name: 'Donuts',
                        state: 'munchies.donuts',
                        type: 'link',
                        icon: 'fa fa-map-marker'
                    }]
                });

                var self;

                return self = {
                    sections: sections,

                    toggleSelectSection: function(section) {
                        self.openedSection = (self.openedSection === section ? null : section);
                    },
                    isSectionSelected: function(section) {
                        return self.openedSection === section;
                    },

                    selectPage: function(section, page) {
                        page && page.url && $location.path(page.url);
                        self.currentSection = section;
                        self.currentPage = page;
                    }
                };

                function sortByHumanName(a, b) {
                    return (a.humanName < b.humanName) ? -1 :
                        (a.humanName > b.humanName) ? 1 : 0;
                }
            }
        ])

})();
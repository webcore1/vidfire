'use strict';

/**
 * @ngdoc service
 * @name uiApp.instancesData
 * @description
 * # instancesData
 * Service in the uiApp.
 */
angular.module('uiApp')
    .service('instancesData', function($http, ENV, $q) {
        // AngularJS will instantiate a singleton by calling "new" on this function

        var instanceService = this;
        var instancesSelected = 'Secret';


        instanceService.getInstances = function() {
            return instances;
        }

        var service = this,
            path = '/api/instances/';

        function getUrl() {
            return ENV.ENDPOINT_URI + path;
        }

        function getUrlForId(itemId) {
            return getUrl(path) + itemId;
        }
        service.all = function() {
            return $http.get(getUrl());
        };
        service.fetch = function(itemId) {
            return $http.get(getUrlForId(itemId));
        };
        service.create = function(item) {
            return $http.post(getUrl(), item);
        };
        service.update = function(itemId, item) {
            return $http.put(getUrlForId(itemId), item);
        };
        service.destroy = function(itemId) {
            return $http.delete(getUrlForId(itemId));
        };
        service.fetchEureka = function(url) {
            return $http.get('http://' + url);
        };

        service.fetchConfig = function( type,name){
            return $http.get(' http://10.120.8.57:8888/api/v1/configuration/components/instances/'+type+'/'+name);
        };

        service.pushConfig = function(url, data) {
            url = 'http://localhost:8888/api/v1/push/';
            return $http.post(url, data);
        };

        //for fetching all configs available for the instance
        service.fetchInstanceConfigs = function(){
           return $http.get('http://10.120.8.57:8888/api/v1/configuration/components/instances');
            //return $http.get('http://localhost:3000/instances');
        };

        service.fetchTemplate = function(name){
            return $http.get('http://10.120.8.57:8888/api/v1/configuration/components/templates/'+name);
        };

        service.configSAVE = function(post,type,name,item ){

            var baseURL = "http://10.120.8.57:8888/api/v1/configuration/components/instances/"+type+"/"+name;

            if(post){
                return $http.post(baseURL,item);
            }else{
                return $http.put(baseURL,item);
            }
        };
        
        service.configPUT = function( type,name,item ){
            return $http.put(' http://10.120.8.57:8888/api/v1/configuration/components/instances/'+type+'/'+name,item);
        };


       service.fetchConfigTemplatesAll = function() {
            return $http.get('http://10.120.8.57:8888/api/v1/configuration/components/templates');
        };


    });
'use strict';

describe('Controller: FileuploaderCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var FileuploaderCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FileuploaderCtrl = $controller('FileuploaderCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(FileuploaderCtrl.awesomeThings.length).toBe(3);
  });
});

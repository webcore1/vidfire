'use strict';

describe('Controller: MonitoringJobsStatusCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var MonitoringJobsStatusCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MonitoringJobsStatusCtrl = $controller('MonitoringJobsStatusCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MonitoringJobsStatusCtrl.awesomeThings.length).toBe(3);
  });
});

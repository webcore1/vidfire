'use strict';

describe('Controller: MonitoringJobsExecutionsCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var MonitoringJobsExecutionsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MonitoringJobsExecutionsCtrl = $controller('MonitoringJobsExecutionsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MonitoringJobsExecutionsCtrl.awesomeThings.length).toBe(3);
  });
});

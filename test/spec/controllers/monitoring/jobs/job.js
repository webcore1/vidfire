'use strict';

describe('Controller: MonitoringJobsJobCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var MonitoringJobsJobCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MonitoringJobsJobCtrl = $controller('MonitoringJobsJobCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MonitoringJobsJobCtrl.awesomeThings.length).toBe(3);
  });
});

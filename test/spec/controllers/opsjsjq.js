'use strict';

describe('Controller: OpsjsjqCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var OpsjsjqCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OpsjsjqCtrl = $controller('OpsjsjqCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OpsjsjqCtrl.awesomeThings.length).toBe(3);
  });
});

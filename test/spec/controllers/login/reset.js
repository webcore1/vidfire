'use strict';

describe('Controller: LoginResetCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var LoginResetCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LoginResetCtrl = $controller('LoginResetCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LoginResetCtrl.awesomeThings.length).toBe(3);
  });
});

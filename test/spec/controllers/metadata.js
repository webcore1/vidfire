'use strict';

describe('Controller: MetadataCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var MetadataCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MetadataCtrl = $controller('MetadataCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MetadataCtrl.awesomeThings.length).toBe(3);
  });
});

'use strict';

describe('Controller: DataflowAlgorithmCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var DataflowAlgorithmCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DataflowAlgorithmCtrl = $controller('DataflowAlgorithmCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DataflowAlgorithmCtrl.awesomeThings.length).toBe(3);
  });
});

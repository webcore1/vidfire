'use strict';

describe('Controller: InstanceconfigCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var InstanceconfigCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InstanceconfigCtrl = $controller('InstanceconfigCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(InstanceconfigCtrl.awesomeThings.length).toBe(3);
  });
});

'use strict';

describe('Controller: SecurityclassCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var SecurityclassCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SecurityclassCtrl = $controller('SecurityclassCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SecurityclassCtrl.awesomeThings.length).toBe(3);
  });
});

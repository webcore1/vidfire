'use strict';

describe('Controller: Customer360Ctrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var Customer360Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Customer360Ctrl = $controller('Customer360Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(Customer360Ctrl.awesomeThings.length).toBe(3);
  });
});

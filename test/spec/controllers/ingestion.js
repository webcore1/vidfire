'use strict';

describe('Controller: IngestionCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var IngestionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IngestionCtrl = $controller('IngestionCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(IngestionCtrl.awesomeThings.length).toBe(3);
  });
});

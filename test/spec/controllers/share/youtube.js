'use strict';

describe('Controller: ShareYoutubeCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var ShareYoutubeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ShareYoutubeCtrl = $controller('ShareYoutubeCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ShareYoutubeCtrl.awesomeThings.length).toBe(3);
  });
});

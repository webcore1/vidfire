'use strict';

describe('Controller: IngestionengineinstancesCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var IngestionengineinstancesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IngestionengineinstancesCtrl = $controller('IngestionengineinstancesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(IngestionengineinstancesCtrl.awesomeThings.length).toBe(3);
  });
});

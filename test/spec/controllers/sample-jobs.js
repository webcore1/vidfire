'use strict';

describe('Controller: SampleJobsCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var SampleJobsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SampleJobsCtrl = $controller('SampleJobsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SampleJobsCtrl.awesomeThings.length).toBe(3);
  });
});

'use strict';

describe('Controller: DataflowCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var DataflowCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DataflowCtrl = $controller('DataflowCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DataflowCtrl.awesomeThings.length).toBe(3);
  });
});

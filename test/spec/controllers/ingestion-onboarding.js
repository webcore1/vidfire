'use strict';

describe('Controller: IngestionOnboardingCtrl', function () {

  // load the controller's module
  beforeEach(module('uiApp'));

  var IngestionOnboardingCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IngestionOnboardingCtrl = $controller('IngestionOnboardingCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(IngestionOnboardingCtrl.awesomeThings.length).toBe(3);
  });
});

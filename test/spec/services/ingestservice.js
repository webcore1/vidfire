'use strict';

describe('Service: ingestservice', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var ingestservice;
  beforeEach(inject(function (_ingestservice_) {
    ingestservice = _ingestservice_;
  }));

  it('should do something', function () {
    expect(!!ingestservice).toBe(true);
  });

});

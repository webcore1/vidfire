'use strict';

describe('Service: algorithmservice', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var algorithmservice;
  beforeEach(inject(function (_algorithmservice_) {
    algorithmservice = _algorithmservice_;
  }));

  it('should do something', function () {
    expect(!!algorithmservice).toBe(true);
  });

});

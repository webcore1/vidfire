'use strict';

describe('Service: SCDF', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var SCDF;
  beforeEach(inject(function (_SCDF_) {
    SCDF = _SCDF_;
  }));

  it('should do something', function () {
    expect(!!SCDF).toBe(true);
  });

});

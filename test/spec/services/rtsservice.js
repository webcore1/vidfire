'use strict';

describe('Service: RTSService', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var RTSService;
  beforeEach(inject(function (_RTSService_) {
    RTSService = _RTSService_;
  }));

  it('should do something', function () {
    expect(!!RTSService).toBe(true);
  });

});

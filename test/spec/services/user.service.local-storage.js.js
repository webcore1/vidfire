'use strict';

describe('Service: user.service.localStorage.js', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var user.service.localStorage.js;
  beforeEach(inject(function (_user.service.localStorage.js_) {
    user.service.localStorage.js = _user.service.localStorage.js_;
  }));

  it('should do something', function () {
    expect(!!user.service.localStorage.js).toBe(true);
  });

});

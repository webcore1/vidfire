'use strict';

describe('Service: dataflowFactory', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var dataflowFactory;
  beforeEach(inject(function (_dataflowFactory_) {
    dataflowFactory = _dataflowFactory_;
  }));

  it('should do something', function () {
    expect(!!dataflowFactory).toBe(true);
  });

});

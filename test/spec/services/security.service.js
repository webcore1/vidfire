'use strict';

describe('Service: security.service', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var security.service;
  beforeEach(inject(function (_security.service_) {
    security.service = _security.service_;
  }));

  it('should do something', function () {
    expect(!!security.service).toBe(true);
  });

});

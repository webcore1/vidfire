'use strict';

describe('Service: operationsData', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var operationsData;
  beforeEach(inject(function (_operationsData_) {
    operationsData = _operationsData_;
  }));

  it('should do something', function () {
    expect(!!operationsData).toBe(true);
  });

});

'use strict';

describe('Service: formly', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var formly;
  beforeEach(inject(function (_formly_) {
    formly = _formly_;
  }));

  it('should do something', function () {
    expect(!!formly).toBe(true);
  });

});

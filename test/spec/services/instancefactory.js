'use strict';

describe('Service: instancefactory', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var instancefactory;
  beforeEach(inject(function (_instancefactory_) {
    instancefactory = _instancefactory_;
  }));

  it('should do something', function () {
    expect(!!instancefactory).toBe(true);
  });

});

'use strict';

describe('Service: onboardservice', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var onboardservice;
  beforeEach(inject(function (_onboardservice_) {
    onboardservice = _onboardservice_;
  }));

  it('should do something', function () {
    expect(!!onboardservice).toBe(true);
  });

});

'use strict';

describe('Service: options', function () {

  // load the service's module
  beforeEach(module('uiApp'));

  // instantiate service
  var options;
  beforeEach(inject(function (_options_) {
    options = _options_;
  }));

  it('should do something', function () {
    expect(!!options).toBe(true);
  });

});
